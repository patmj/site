# 🚧 **SITE EN CONSTRUCTION** 🚧

Le but de ce site est de proposer un ensemble de cours, ressources et liens pour l'enseignement de l'informatique au lycée. Il regroupe de nombreuses ressources (cours, TP, exercices, projets) dont un grand nombre sont des reprises ou des adaptations d'activités créées et partagées par d'autres professeurs de NSI et SNT que je remercie chaleureusement 👏.


Consultable à l'adresse: https://patmj.forge.apps.education.fr/site/ 

