# Comment utiliser GitHub pour collaborer sur un projet de développement ?

Auteur : Rafael GRAFF

Que ce soit à l’école, au travail ou sur notre temps libre, nous devons parfois développer un programme en collaboration avec quelqu’un d’autre. Dans ces situations là, trouver la bonne méthode pour aisément partager le code est une tâche embêtante. À travers ce tutoriel, nous allons vous montrer comment régler ce problème avec GitHub, et notamment leur outil *GitHub Desktop*.

1. Qu’est-ce que GitHub ?
2. La méthode que nous allons utiliser
3. L’installation de GitHub Desktop
4. Configuration
5. Modifier le code et le publier


## Qu'est-ce que GitHub ?
Dit de façon simple, GitHub est un site web qui permet aux développeurs de stocker et gérer leur code. C'est entièrement gratuit pour une utilisation personelle.

GitHub s'appuie en réalité sur le logiciel Git, qui a des fonctionnalités très poussées comme le versionnage ou le forking. Toutefois, nous ne l'aborderons pas dans ce tutoriel.


## La méthode que nous allons utiliser
Dans ce tutoriel, je nous allons utiliser GitHub pour créer un dépôt (*"repository" en anglais*) qui contiendra tout notre code source, puis comment installer et configurer GitHub Desktop pour modifier ce code directement depuis l'ordinateur comme si l'on modifiait un fichier local.

## 1. Créer son premier dépôt GitHub

Rendez-vous sur [https://github.com/signup](https://github.com/signup) et suivez les consignes pour vous créer un compte GitHub. Connectez-vous directement si vous en avez déjà un.

Une fois sur la page d'accueil, cliquez sur le bouton "+" en haut à droite puis, dans le menu déroulant, cliquez sur "`New repository`"  

![](../images/1-NewRepository.png)

Remplissez le nom du dépôt, la description, et choisissez si votre dépôt doit être public (accessible à tous) ou privé (seulement à ceux que vous avez ajouté). Il y a quelques paramètres supplémentaires avec lesquels vous pouvez expérimenter comme "Add a README file" ou "Choose a license".

Une fois que tout est fait : ***tada*** ! Vous êtes maintenant sur la page de votre projet.

## 2. Ajouter des collaborateurs
Tout cela est bien beau, mais actuellement nous n'avons qu'un dépôt vide. Ajoutons-y des amis !

Toujours sur la page d'accueil de votre projet, jetez un coup d'oeil à la barre d'outil du haut. Elle s'étend sur deux lignes : la plus haute ayant des boutons pour GitHub en général, et la plus basse vous permettant d'accéder aux différentes pages de votre dépôt. Sur celle du bas, nous allons donc nous cliquer sur le bouton "Settings" pour accéder aux paramètres.

![](../images/2-MainPage.png)

Vous êtes maintenant sur la page des paramètres de votre dépôt. Ici, tout est possible ; libre à vous d'explorer les différentes possibilités.

Pour l'instant, nous voulons ajouter des collaborateurs. À gauche, trouvez le bouton *"Collaborators"* et cliquez dessus.

![](../images/3-Collaborators.png)

Appuyez ensuite sur le bouton vert *"Add people"*. Une page devrait apparaître avec une barre de recherche au millieu.
Dans la barre de recherche, entrez le nom d'utilisateur ou l'e-mail de votre collaborateur. Cliquez ensuite sur *"Add ... to this repository"*.

Vous pouvez répéter cette action jusqu'à ce que tous les collaborateurs soient invités. Si l'un de vos collaborateur n'est pas encore inscrit, vous pourrez revenir sur cette page plus tard et l'inviter.

![](../images/4-AddColab.gif)

Chacun du/des collaborateur(s) que vous avez invité peut alors se rendre sur [https://github.com/notifications](https://github.com/notifications) pour retrouver l'invitation de collaboration. Il est possible que l'invitation prenne un peu de temps à arriver. Patientez, prenez un thé, buvez de l'eau, tout va bien se passer.

Après avoir ouvert le message d'invitation, il doit cliquer sur "Accept invitation"

![](../images/3A-Invite.png)
![](../images/3B-AcceptInvite.png)

Maintenant, il est temps de passer aux choses sérieuses. On veut pouvoir modifier notre dépôt depuis notre ordinateur, et le traîter comme si c'était un dossier local.

## 3. Cloner le dépôt sur notre ordinateur
GitHub Desktop est un logiciel développé par GitHub qui permet de faire apparaître son dépôt en tant que répertoire sur l'ordinateur, pour pouvoir facilement l'exécuter ou le modifier avec d'autres logiciels.

Pour l'installer, rendez-vous sur [https://desktop.github.com/](https://desktop.github.com/) et cliquez sur le bouton  "Download for Windows" au centre. Ouvrez ensuite le fichier téléchargé et suivez les étapes de l'installtion.
Une fois l'installation finie, ouvrez GitHub Desktop.

GitHub Desktop devrait vous accueillir avec un chouette page d'introduction.
Sur celle-ci, plusieurs options seront possible ; le bouton qui va nous être utile, celui pour cloner le dépôt sur notre machine est intitulé *"Clone a repository from the Internet..."*. Cliquez dessus, puis sous "Your repositories" cliquez sur le dépôt qui vous intéresse. Dans mon cas c'est `GraffAir/testrep` (GraffAir étant mon nom d'utilisateur et testrep celui du dépôt).

Une fois le dépôt selectionné, cliquez sur "Clone". L'option "Local path" vous permet de changer l'emplacement où sera placé le dossier sur votre ordinateur. Pour ma part, j'ai décidé de le laisser dans `Documents\GitHub`.

Cliquez ensuite sur "Clone" pour continuer.

![](../images/5-CloneRepo.gif)

## Notre première modification
Ouf, on a enfin tout configuré. Si ça s'est bien passé, votre application GitHub Desktop devrait vous montrer une toute nouvelle page avec marqué "No local changes" en gros avec trois options en dessous. Si ce n'est pas le cas, relisez les dernières parties pour voir ce que vous avez manqué. ~~Si les symptômes persistent, consultez votre médecin.~~

Il est enfin l'heure de faire notre toute première modification à notre dépôt.
Pour ça, cliquez sur le bouton "Show in Explorer" sous le titre ou appuyez sur `Ctrl + Shift + F`. Vous serez emmené sur le dossier du dépôt.
Ici vous aurez un dossier vide (ou presque). Créez votre premier fichier : ce que vous voulez.
Je vais créer un fichier Python (en .py) pour la facilité, mais notez qu'ici ce sont les fichiers de votre projet qui vont être placés. Modifions également le fichier pour y ajouter un texte quelconque, comme `print("Hello World!")`

Après avoir créé notre fichier et inséré un texte quelconque dedans, retournons sur github.com pour voir notre dépôt et observer notre magnifique modification.

Mais catastrophe ! Le fichier n'apparaît pas ! Sommes-nous maudits ?

Mais non enfin, nous n'avons juste pas publié votre modification !

Pour que tous le monde puisse profiter de cette modification, et qu'elle soit visible sur la page github.com de votre dépôt, vous devez la publier. Pour se faire, retournons sur GitHub Desktop. Vous observerez que la page a changée depuis la dernière fois : elle affiche toutes les modifications que vous avez faites. Observons un peu plus en détail cette page :

- À gauche se trouve un menu listant chaque fichier qui a été modifié dans le dépôt. Vous pouvez cocher ou décocher individuellement chaque fichier pour choisir de publier ou non les modifications apportées. En parlant de modifications apportées...
- Le grand espace à droite présente les modifications concrètes apportées au fichier selectionné. Lignes enlevées, ajoutées, modifiées...
- En dessous de la liste des fichiers se trouve une petite fenêtre où vous pouvez décrire la modification que vous avez fait. Le titre est obligatoire mais la description, elle, est entièrement facultative.
- Enfin, tout en bas à gauche, un bouton pour appliquer la modification à la branche principale "Commit to **main**".

> Git, le programme sur lequel GitHub se base, utilise un système de branche pour les modifications. Quand vous modifiez un élément sur votre dépôt, une nouvelle branche temporaire est créée qui entrepose tous vos changements jusqu'à ce que vous les transfériez à nouveau sur la branche principale, alias le bouton "Commit to **main**". [En savoir plus sur les branches ici](https://docs.github.com/fr/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-branches).

![](../images/6-Commit.png)

Nous avons donc rempli le titre et la description de notre modification puis cliqué sur "Commit to main". Maintenant, il nous reste à publier la nouvelle branche sur GitHub.

Pour se faire, rien de plus simple. En haut de l'écran, trouvez le bouton "Publish branch" et cliquez dessus. Il y aura un petit chargement puis tadaa, le changement est là !

![](../images/7-Publish.png)

Si vous retournez sur la page de votre dépôt et que vous actualisez la page, vous observerez que vos modifications sont présentes en chair et en os.

## Télécharger les modifications d'internet

Une dernière chose toutefois : si le dépôt est modifié par quelqu'un d'autre, ou même par vous depuis le site web, les changements ne seront plus les même que sur votre ordinateur !

Pour illustrer ça, nous allons nous-même modifier le fichier que nous venons de créer, mais cette fois depuis le site web. Sur la page d'accueil, vous avez une vue d'ensemble de votre dépôt avec les fichiers et dossiers présents à l'intérieur. Cliquez sur le nom du fichier que vous venez de créer. Pour ma part, c'est `hello.py`.

![](../images/8-EditOnline.png)

Vous verrez le contenu du fichier que vous venez d'ouvrir. Si c'est un fichier code (.py, .js, etc...) vous verrez le code coloré selon la syntaxe appropriée.
Modifiez le fichier en cliquant sur l'icone de crayon à droite de la fenêtre contenant le  code.

![](../images/9-EditFile.png)

Faites n'importe quelle modification et cliquez sur "Commit changes".

![](../images/10-CommitChangesA.png)

Comme vous avez fait sur GitHub Desktop, décrivez votre modification puis cliquez à nouveau sur "Commit changes".

![](../images/10B-CommitChangesB.png)
 
Si vous retournez maintenant sur votre dépôt local (sur votre ordinateur), cette modification n'apparaîtra pas malgré qu'elle ait correctement été publiée sur GitHub.

C'est parce que vous n'avez pas récupéré les modifications faites à distance. C'est assez simple, car le bouton est à la même place que celui pour publier : en haut à droite, cliquez sur "Fetch origin" qui comparera la version de votre ordinateur à celle sur `github.com`. 
Si des modifications ont été faites qui n'ont pas été téléchargées sur votre ordinateur, le bouton chargera puis affichera "Pull origin". Cliquez une nouvelle fois dessus pour télécharger la modification.

> - **Fetch origin** cherche la version actuellement publiée sur GitHub pour y voir quelconque modification avec celle présente sur votre ordinateur.
> - **Pull origin** "tire" (télécharge) ces modifications vers votre machine.

N'oubliez pas d'actualiser votre version locale avant de vous mettre à programmer. Si vous attendez le moment de la publication pour vous rendre compte des modifications qui avaient déjà été faites, c'est la porte ouverte aux problèmes !

![](../images/11-Fetchorigin.png)

Et voilà ! Vous avez appri à créer un dépôt, le publier, le configurer, le cloner sur votre machine et vous avez géré tout ça comme un pro !
En espérant avoir pu vous aider, **tschuss** !

Auteur : Rafael GRAFF

Dernière modification : Novembre 2023

Licence [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/fr/)

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
