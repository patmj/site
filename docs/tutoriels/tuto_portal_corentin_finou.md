<style>
    p{
        font-size: 18px;
    }
</style>


<center><div style="float: none;width: 25%;"><img src="../../images/LogoTouPo.png" alt="Logo"></div></center>


<center><h1>Utiliser l'application de contrôle de macro Touch Portal</h1></center>
<br>Auteur : Corentin FINNOU
<br><br>
<p style="font-size: 18px;">
Lorsqu'on utilise son ordinateur pour effectuer certaines choses, il est possible dans de nombreux cas de devoir répéter une ou plusieurs mêmes actions. De ce problème sont nées les macros, basiquement des programmes permettant de programmer des actions précises puis de les effectuer automatiquement, rendant la tâche plus rapide et plus simple.<br>
On peut retrouver de nombreux logiciel ou plugins sur le net permettant cela, dans certains cas, ce principe est même directement intégré. En revanche, il est facile de se perdre parmi le nombre de paramètres, les interfaces pas toujours intuitives ou encore la gestion de fichiers que cela peut demander. De plus, selon comment ces logiciels sont pensés, il est alors parfois possible que la mise en place et l'utilisation de la macro soit plus longue que si on avait fait l'action nous même.
</p>
<br>
<h2>Qu'est-ce que Touch Portal ?</h2>
<br>
<p style="font-size: 18px;">
    Le logiciel et application <b>Touch Portal</b> permet de programmer en bloc vos macros et de les déclencher depuis votre téléphone grâce à des boutons personnalisables. 
</p>
<img src="../../images/exemple.png" width="40%" style="float: right;">
<p style="font-size: 18px;">
    <br><br>
    La version gratuite vous permettra de créer jusqu'a 14 boutons utilitaires (2 pages de 4x2 boutons dont deux pour la navigation entre les pages), pouvant être programmés pour effectuer des actions variées :<br>
  -Executer et ouvrir des fichiers/dossiers/applications<br>
  -Naviguer sur internet<br>
  -Effectuer des actions de clavier/souris<br>
  -Verrouiller ou éteindre l'ordinateur<br>
  -Gérer les sons et médias<br>
  -...<br>
</p>
    
<table style="border: 1px solid #333;font-size: 20px;">
    <tr>
        <th colspan="2" style="background-color: rgb(77,77,77);"><center>Versions</center></th>
    </tr>
    <tr style="border: 1px solid #333;">
        <td style="border: 1px solid #333;">
            <center>
                <b>Version de base</b>
                <br>
                Couleur de fond par défaut<br>
                Pas d'image de fond<br>
                Page de 8 boutons maximum<br>
                2 Pages maximum
            </center>
        </td>
        <td style="border: 1px solid #333;">
            <center>
                <b>Version Pro</b>
                <br>
                Couleur de fond personnalisable<br>
                Image de fond personnalisable<br>
                Page de 110 boutons maximum<br>
                Nombre de page illimité
            </center>
        </td>
    </tr>
</table>
<br>
<h2>Installer Touch Portal</h2>
<br>

<p style="font-size: 18px;">
    L'installation de Touch Portal se fait en deux temps : l'installation du logiciel sur votre ordinateur puis de l'application sur votre téléphone. L'ordre n'est pas important, mais il est recommandé de ne lancer l'application qu'une fois que le logiciel est bien installé.<br>
    <br>
    <u>Installation sur ordinateur :</u><br><br>
    -Se rendre sur le site officiel de <a href="https://www.touch-portal.com">Touch Portal</a> :<br>
    <img src="../../images/Liensite.png" width="40%"><br><br>
    -Télécharger Touch Portal en fonction de votre système d'exploitation :<br>
    <img src="../../images/DownloadTouchPortal.png" width="40%"><br><br>
    -Lancer le fichier setup téléchargé :
    <img src="../../images/FichierSetup.png" width="80%"><br><br>
    -Autoriser les modifications.<br><br>
    -Accepter les conditions d'utilisations.<br><br>
    -Choisir le dossier d'installation.<br><br>
    -Suivre les instructions.<br><br>
    -Redémarrer l'ordinateur si demandé.<br><br><br>
    <u>Installation sur téléphone :</u><br><br>
    -Téléchager l'application touch portal depuis le site ou votre app store.<br>
    <img src="../../images/playstore.jpg" width="40%"><br><br>
    -Lancer l'application jusqu'à l'écran "install software".<br><br>
    -Une fois le logiciel sur votre ordinateur lancé, appuyer sur continuer.<br><br>
    -Effectuer le scan, si tout est bien configuré, la connexion se fera automatiquement, autrement, regarder dans les paramètres de connexion.<br><br>
</p>
<br>
<h2>Utiliser Touch Portal</h2>
<br>
<p style="font-size: 18px;">
    Une fois que le logiciel et l'application sont connectés, vous arriverez sur cette page, similaire à ce qui sera affiché sur votre téléphone :<br>
    <center>Sur votre ordinateur :</center>
    <img src="../../images/Pagedebase.png" width="40%"><br><br>
    <center>Sur votre téléphone :</center>
    <img src="../../images/exempleTel.jpg" width="40%"><br><br>
</p>
<p style="font-size: 18px;">
    Vous pourrez alors commencer à créer vos boutons, aux endroits que vous souhaitez, et constater en temps réel les modifications faites sur votre téléphone.<br>
    En cliquant sur les cases disponibles sur votre page logicielle, vous aurez la possibilité de paramétrer votre bouton (taille, couleur, texte, image,...) ainsi que la macro grâce aux nombreux blocs disponibles :<br>
    <img src="../../images/Actions.png" height="40%"><br><br>
    Après avoir donné (ou pas) un nom à la page par défault (initialement "main"), vous pourrez créer une deuxième page, un bouton sera automatiquement créé sur cette dernière, permettant le retour vers "main", l'inverse demandera en revanche une création manuelle, ne vous inquiétez donc pas si lors de la création de votre deuxième page, rien ne s'affiche sur votre téléphone, il faudra simplement créer vous-même un bouton de navigation grâce au bloc "aller à la page {nom}".
</p>
<br>
<h2>Conseils</h2>
<br>
<p style="font-size: 18px;">
    L'application aura tendance à activer un mode d'économie de batterie, éteignant l'écran jusqu'au prochain appui. Lors d'une utilisation rare de vos boutons, cela n'est pas dérangeant, en revanche, si vous préférez que votre écran reste allumé, vous pouvez alors configurer le paramètre "Type of screensaver" sur none. Faites simplement attention aux possibles surchauffes de votre téléphone, spécifiquement si vous le laissez en charge pendant l'utilisation de touch portal.<br><br>
    Lors de la création de bouton, il est conseillé de directement paramétrer un moyen de le reconnaître (une simple lettre comme nom de bouton suffit) afin de ne pas galerer lors des possibles essais.<br><br>
</p>
<p> Corentin Finou
<br>
Dernière modification du document : Novembre 2023
<br>
</p>  

Licence [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/fr/)

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)    
  