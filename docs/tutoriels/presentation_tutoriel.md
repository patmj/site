# <span style="color: #E49B0F">Tutoriels</span>

Auteur: Patrice MARIE-JEANNE

Nous reprenons ici l'[excellente idée de Vincent Robert](https://nsi.xyz/category/tutoriels/), professeur de NSI du lycée Louis Pasteur d'Avignon.

Avant de vous lancer dans la création d'un tutoriel, assurez-vous de réaliser un travail utile pour la communauté éducative !

À privilégier :

* logiciels ou matériels libres ou open source (FOSS);
* logiciels ou matériels pour lequels il n'existe pas de tutoriels en français accessibles pour un lycéen

Vous repecterez le [modèle fourni](./modele_tutoriel.md).
