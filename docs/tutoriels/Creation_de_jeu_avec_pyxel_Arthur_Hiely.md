<h1>Tutoriel : Faire un jeu simple en utilisant Pyxel (Terminale)<br></h1>

Auteur : Arthur Hiely

Ce tutoriel aura pour but de vous permettre de créer votre premier jeu en Python en utilisant le module Pyxel. Dans ce tuto, vous trouverez les bases du codage avec Pyxel, ainsi que des exemples d'utilisations.
<br>Je vais vous montrer ici les étapes pour créer un jeu dans lequel le joueur se déplace sur un chemin et tire des missiles sur des ennemis qui avancent vers lui.
<br>Pour coder en pyxel, il est recommandé d'aller sur https://www.pyxelstudio.net et d'appuyer sur le bouton create, car certains Idle ne prennent pas en charge le module.

![](../images/pyxel_TUTO_4.png)

<h2>Première étape : Créer une fenêtre </h2>

En Pyxel, pour créer une fenêtre, on utilise pyxel.init(), qui prend comme paramètres obligatoires la longueur, la hauteur, puis en optionnels le titre, un nombre de fps... -> Exemple : `pyxel.init(128, 128, title="Jeu")`
<br>Ensuite, nous allons créer une classe `Jeu`, qui contiendra tout le code de notre jeu.
<br>Dans l'initialisation nous allons lancer la fenêtre, puis lancer la boucle du jeu avec `pyxel.run()`, qui prend en paramètres ce qu'il faut actualiser à chaque instant, et ce qu'il faut dessiner.
<br>Il faut donc créer ces deux méthodes dans la classe, l'une contenant tout ce qu'il faut actualiser, l'autre tout ce qu'il faut dessiner.

```python
import pyxel
class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #lance le jeu
        pyxel.run(self.update, self.draw)
        
    def update(self):
        """mise à jour des variables"""
        pass
        
    def draw(self):
        """création et positionnement des objets"""
        pass
```

Voici donc pour l'instant l'état de notre jeu.
<br>Pour l'instant, dans `update` on ne met "rien" (un `pass`) car rien n'est à actualiser, il n'y a aucun objet à gérer.
<br>Par contre, si on lance le programme en l'état, rien ne sera dessiné et ce n'est pas ce qu'on veut.
<br>Pour ce jeu, je voudrais remplir le fond en vert afin de simuler un pré. Pour cela, on utilise `pyxel.cls()`, qui va dans un premier temps vider la fenêtre de tout les objets dessinés, puis remplir le fond de la couleur rentrée en paramètre. Les couleurs sont sous formes de nombres de 0 à 15. (le vert utilisé correspond a 11).
<br>Et je voudrais aussi créer un rectangle marron dans la partie inférieur de la fenêtre, afin de simuler un chemin. Pour cela on utilise `pyxel.rect()`, qui prend en paramètres une position x et y, la longueur et la hauteur de l'objet, ainsi que sa couleur.

On ajoute donc dans `draw` :

```python
#met le fond en vert
pyxel.cls(11)
#crée un rectangle marron pour simuler le chemin
pyxel.rect(0, 40, 128, 40, 9)
```

Le code ressemble donc à cela pour l'instant :

```python
import pyxel
class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #lance le jeu
        pyxel.run(self.update, self.draw)
        
    def update(self):
        """mise à jour des variables"""
        pass
        
    def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
```

<h2>Deuxième étape : Créer un personnage et pouvoir le deplacer </h2>

Il va falloir tout d'abord créer 2 variables qui contiennent les coordonnées du joueur.
<br>Ensuite, nous créerons une méthode `déplacement()` qui permettra de changer les coordonnées du joueur à l'aide des touches du clavier, puis d'actualiser cette méthode toutes les secondes en la rajoutant dans update(), puis de dessiner le personnage aux coordonnées des variables chargés de le localiser dans `draw()`.
<br>Avec Pyxel, afin de récupérer les touches préssés du clavier, on utilise `pyxel.btn()`, avec comme paramètre la touche en question, sous cette forme : `pyxel.KEY_RIGHT` pour récupérer la fléche droite par exemple.
<br>Voici un exemple d'utilisation 
<br>
```python
if pyxel.btn(pyxel.KEY_RIGHT):
    print("fléche droite pressée")
```

Dans la méthode `deplacement()`, on modifiera ensuite les valeurs des coordonnées x et y si les touches directionnelles sont pressées. On ajoutera aussi une condition pour que le joueur se déplace, afin de l'empêcher de sortir du chemin tracé précedémment.

Puis dans `update()` on ajoute la mise à jour constante de la méthode deplacement, et dans `draw()`, on ajoute qu'il faudra dessiner un rectangle qui prendra comme point de départ les coordonnées du joueur et qui fera 2 pixels de longueur et 4 de hauteur. Sa couleur sera marron.

```python
import pyxel
from random import*

class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #définir la position du joueur
        self.pos_x = 20
        self.pos_y = 60
        #lance le jeu
        pyxel.run(self.update, self.draw)
    
    def deplacement(self):
        """gérer les déplacements avec les flèches directionnelles"""
        if pyxel.btn(pyxel.KEY_RIGHT) and self.pos_x<126:
            self.pos_x += 2
        if pyxel.btn(pyxel.KEY_LEFT) and self.pos_x>0:
            self.pos_x -= 2
        if pyxel.btn(pyxel.KEY_DOWN) and self.pos_y<76:
            self.pos_y += 2
        if pyxel.btn(pyxel.KEY_UP) and self.pos_y>40:
            self.pos_y -= 2

    def update(self):
        """mise à jour des variables"""
        self.deplacement()
        
    def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
        #dessiner le joueur
        pyxel.rect(self.pos_x, self.pos_y, 2, 4, 4)
```

Ps : Ici, lorsque l'on presse la touche fléche vers le bas, on augmente la valeur de y, car dans le domaine du jeu vidéo en 2D, l'origine des deux axes n'est pas en bas à gauche, mais en haut à gauche.
<br>Et donc, lorsque l'on presse la touche du bas, pour faire descendre le joueur, on ajoute bien et on ne soustrait pas.

![](../images/pyxel_TUTO_3.png)

<h2>Troisième étape : Créer des ennemis </h2>

Pour cela, on commence par créer une liste vide dans l'initialisation de classe, qui va contenir tous les ennemis qui sont sur le terrain.
<br>Ensuite on crée une méthode `creation_ennemis()`, qui permettra de faire apparaître les ennemis aléatoirement 
à intervalle régulier.
<br>Pour cela, il faudra utiliser `pyxel.frame_count` qui permet de mesurer le temps, ainsi que le module `random` pour gérer l'aléatoire.
<br>Dans ce jeu, les ennemis apparaitront toute les 1,5 secondes, pour cela on utilise cette ligne :

--> `if (pyxel.frame_count % 45 == 0):`. Puis en dessous les instructions à effectuer toutes les 1,5 secondes.
<br>(`pyxel.frame_count % 30 == 0` permet de faire les actions toutes les 1 secondes, `frame_count % 15 == 0` toutes les 0,5 secondes ...)

A la liste des ennemis on ajoutera donc toutes les 1,5 secondes une liste de deux élements, représentant les coordonnées de l'ennemi en question.
<br>Il apparaitront toujours à la même abscisse, seule leur ordonnée variera. Ils apparaitront dans le rectangle que l'on a déjà défini représentant le chemin. 
<br>On ajoutera donc cette méthode à notre classe :

```python
def creation_ennemis(self):
        """création aléatoire des ennemis"""
        if (pyxel.frame_count % 45 == 0):
            self.ennemis.append([125, randint(35, 72)])
```

On la mettra à jour dans `update()`, puis on dessinera chaque ennemi dans `draw()`. Ils feront 3 de longueur et 6 de hauteur.

```python
import pyxel
from random import *

class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #définir la position du joueur
        self.pos_x = 20
        self.pos_y = 60
        #crée les liste contenant les ennemis
        self.ennemis = []
        #lance le jeu
        pyxel.run(self.update, self.draw)
    
    def deplacement(self):
        """gérer les déplacements avec les flèches directionnelles"""
        if pyxel.btn(pyxel.KEY_RIGHT) and self.pos_x<126:
            self.pos_x += 2
        if pyxel.btn(pyxel.KEY_LEFT) and self.pos_x>0:
            self.pos_x -= 2
        if pyxel.btn(pyxel.KEY_DOWN) and self.pos_y<76:
            self.pos_y += 2
        if pyxel.btn(pyxel.KEY_UP) and self.pos_y>40:
            self.pos_y -= 2
            
    def creation_ennemis(self):
        """création aléatoire des ennemis"""
        if (pyxel.frame_count % 45 == 0):
            self.ennemis.append([125, randint(40, 74)])

    def update(self):
        """mise à jour des variables"""
        self.deplacement()
        self.creation_ennemis()
        
    def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
        #dessiner le joueur
        pyxel.rect(self.pos_x, self.pos_y, 2, 4, 4)
        #dessiner les ennemis
        for ennemi in self.ennemis:
            pyxel.rect(ennemi[0], ennemi[1], 3, 6, 8)
'''

Donc les ennemis sont rajoutés, mais ils restent immobiles.
<br>Afin de les faire se déplacer, on rajoute une nouvelle méthode qui aura pour but de diminuer de 1 la coordonnée x de chaque ennemi.
<br>On ajoute aussi une ligne afin de les faire disparaitre de la liste des ennemis s'ils sortent de l'écran.
<br>Et on oublie pas également de l'actualiser dans `update()`

```python
def deplacement_ennemis(self):
        """déplacement des ennemis"""              
        for ennemi in self.ennemis:
            ennemi[0] -= 1
            if ennemi[0]<0:
                self.ennemis.remove(ennemi)
```              

```python
import pyxel
from random import *

class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #définir la position du joueur
        self.pos_x = 20
        self.pos_y = 60
        #crée les liste contenant les ennemis
        self.ennemis = []
        #lance le jeu
        pyxel.run(self.update, self.draw)
    
    def deplacement(self):
        """gérer les déplacements avec les flèches directionnelles"""
        if pyxel.btn(pyxel.KEY_RIGHT) and self.pos_x<126:
            self.pos_x += 2
        if pyxel.btn(pyxel.KEY_LEFT) and self.pos_x>0:
            self.pos_x -= 2
        if pyxel.btn(pyxel.KEY_DOWN) and self.pos_y<76:
            self.pos_y += 2
        if pyxel.btn(pyxel.KEY_UP) and self.pos_y>40:
            self.pos_y -= 2
            
    def creation_ennemis(self):
        """création aléatoire des ennemis"""
        if (pyxel.frame_count % 45 == 0):
            self.ennemis.append([125, randint(40, 74)])
    
    def deplacement_ennemis(self):
        """déplacement des ennemis"""              
        for ennemi in self.ennemis:
            ennemi[0] -= 1
            if ennemi[0]<0:
                self.ennemis.remove(ennemi)

    def update(self):
        """mise à jour des variables"""
        self.deplacement()
        self.creation_ennemis()
        self.deplacement_ennemis()
        
    def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
        #dessiner le joueur
        pyxel.rect(self.pos_x, self.pos_y, 2, 4, 4)
        #dessiner les ennemis
        for ennemi in self.ennemis:
            pyxel.rect(ennemi[0], ennemi[1], 3, 6, 8)
```

<h2>Quatrième étape : Ajouter des tirs avec une pression sur la barre espace </h2>

Pour cela, on crée une nouvelle liste vide qui va contenir les missiles, puis on ajoute une nouvelle méthode, qui ajoute un missile à la liste.



Cette fois on utilise `pyxel.btnr()` à la place de `pyxel.btn()`, ce qui empêche de maintenir la touche. Ainsi le joueur devra à chaque fois arrêter d'appuyer sur espace puis reappuyer pour tirer.
<br>Chaque missile tiré est ajouté à la liste et son point de départ sera juste à coté du joueur.

```python
def creation_tirs(self):
        """crée un tir avec Espace"""
        if pyxel.btnr(pyxel.KEY_SPACE):
            self.tirs.append([self.pos_x+2, self.pos_y+1])
```           

Puis de la même manière qu'avec les ennemis, on déplace les missile en créant une méthode.

```python
def deplacement_tirs(self):
        """déplacement des tirs"""
        for tir in self.tirs:
            tir[0] += 1
            if tir[0]>128:
                self.tirs.remove(tir)
```                

Puis on actualise dans `update()` et on les dessines dans `draw()`.

```python
import pyxel
from random import *

class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #définir la position du joueur
        self.pos_x = 20
        self.pos_y = 60
        #crée les liste contenant les tirs et les ennemis en cours
        self.tirs = []
        self.ennemis = []
        #lance le jeu
        pyxel.run(self.update, self.draw)
    
    def deplacement(self):
        """gérer les déplacements avec les flèches directionnelles"""
        if pyxel.btn(pyxel.KEY_RIGHT) and self.pos_x<126:
            self.pos_x += 2
        if pyxel.btn(pyxel.KEY_LEFT) and self.pos_x>0:
            self.pos_x -= 2
        if pyxel.btn(pyxel.KEY_DOWN) and self.pos_y<76:
            self.pos_y += 2
        if pyxel.btn(pyxel.KEY_UP) and self.pos_y>40:
            self.pos_y -= 2
            
    def creation_tirs(self):
        """crée un tir avec Espace"""
        if pyxel.btnr(pyxel.KEY_SPACE):
            self.tirs.append([self.pos_x+2, self.pos_y+1])
        
    def deplacement_tirs(self):
        """déplacement des tirs"""
        for tir in self.tirs:
            tir[0] += 1
            if tir[0]>128:
                self.tirs.remove(tir)
                
    def creation_ennemis(self):
        """création aléatoire des ennemis"""
        if (pyxel.frame_count % 45 == 0):
            self.ennemis.append([125, randint(40, 74)])
            
    def deplacement_ennemis(self):
        """déplacement des ennemis"""              
        for ennemi in self.ennemis:
            ennemi[0] -= 1
            if ennemi[0]<0:
                self.ennemis.remove(ennemi)

    def update(self):
        """mise à jour des variables"""
        self.deplacement()
        self.creation_tirs()
        self.deplacement_tirs()
        self.creation_ennemis()
        self.deplacement_ennemis()
        
    def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
        #dessiner le joueur
        pyxel.rect(self.pos_x, self.pos_y, 2, 4, 4)
        #dessiner les tirs
        for tir in self.tirs:
            pyxel.rect(tir[0], tir[1], 4, 1, 10)
        #dessiner les ennemis
        for ennemi in self.ennemis:
            pyxel.rect(ennemi[0], ennemi[1], 3, 6, 8)
```

<h2>Dernière étape : Gérer les collisions </h2>

Dans cette étape, on va faire en sorte que si un missile touche un ennemi celui ci disparait, que si le joueur touche un ennemi, il perd une vie ; on va aussi créer un compteur d'ennemis tués.

Pour cela, on commence par créer deux variables de vies et de score vides, puis créer une méthode `joueur_touche()` pour faire perdre une vie au joueur, une méthode `ennemi_touche()` pour faire disparaitre le missile ainsi que l'ennemi qu'il a touché.

On va donc s'attaquer aux collision, mais il va falloir éclairer certains points avant de commencer.
<br>Tout d'abord, il ne suffit pas de mettre :

```python
for ennemi in self.ennemis:
    if self.pos_x == ennemi[0]:
        print("joueur touché")
```              
<br>Car lorsqu'on parle des coordonnées des deux objets, on ne considère pas les objets dans leur ensemble. En fait, pour chacun des deux objets, les coordonnées sont celles du pixel situé dans le coin en haut à gauche. 
<br>Ce qui fait que dans la deuxième image ci-dessous par exemple, de notre point de vue les deux objets se touchent, alors que quand on considère leurs pixels en haut à gauche, ils ne se touchent pas, et le jeu ne les considère pas comme en collision.

![](../images/pyxel_TUTO_1.png)

![](../images/pyxel_TUTO_2.png)

Et donc, pour vérifier s'ils sont en contact, il faut faire une sorte de zone autour du pixel considéré de l'objet 2 et voir si le pixel de l'objet 1 s'y trouve.
<br>Voici le code, qui va vérifier si le pixel sur joueur est dans la zone autour de l'ennemi, dans les coordonnées x et y.

```python
def joueur_touche(self):
        """enclenche la fin du jeu si le joueur est touché"""
        for ennemi in self.ennemis:
            if ennemi[0]+2 >= self.pos_x >= ennemi[0]-1 and ennemi[1]+5 >= self.pos_y >= ennemi[1]-3:
                self.ennemis.remove(ennemi)
                self.vies -= 1
```                

On peut faire la même chose avec le tir qui heurte un ennemi.

```python
def ennemis_touche(self):
        """disparition d'un ennemi lorsque contact avec le tir"""
        for ennemi in self.ennemis:
            for tir in self.tirs:
                if tir[1] >= ennemi[1] >= tir[1]-5 and ennemi[0] <= tir[0]+3:
                    self.ennemis.remove(ennemi)
                    self.tirs.remove(tir)
                    self.score += 1
```                    

On rajoute donc dans `update()` ces deux méthodes pour les mettre à jour, puis on modifie `draw()` :


```python
def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
        #afficher le score et les vies
        pyxel.text(2, 2, f"SCORE : {self.score}", 10)
        pyxel.text(94, 2, f"VIES : {self.vies}", 12)
        #si le joueur n'est pas touché
        if self.vies > 0:
            #dessiner le joueur
            pyxel.rect(self.pos_x, self.pos_y, 2, 4, 4)
            #dessiner les tirs
            for tir in self.tirs:
                pyxel.rect(tir[0], tir[1], 4, 1, 10)
            #dessiner les ennemis
            for ennemi in self.ennemis:
                pyxel.rect(ennemi[0], ennemi[1], 3, 6, 8)
        else:
            #afficher GAME OVER si perdu
            pyxel.text(48, 20, 'GAME OVER', 8)
            #vider le contenu des listes d'ennemis et de tirs
            for ennemi in self.ennemis:
                self.ennemis.remove(ennemi)
            for tir in self.tirs:
                self.tirs.remove(tir)
```                

`pyxel.text(2, 2, f"SCORE : {self.score}", 10)` et `pyxel.text(94, 2, f"VIES : {self.vies}", 12)` permettent d'afficher un texte (`pyxel.text()` prend en paramètre les coordonnées x et y, ainsi que le texte qui sera affiché et la couleur)
<br>Ensuite on ne dessine le joueur, les ennemis et les tirs que si le joueur a des vies.
<br>Et sinon, on affiche le texte Game Over et on supprime les missiles tirés et les ennemis des listes.

Voici le code complet.

```python
import pyxel
from random import *

class Jeu:
    def __init__(self):
        """initialisation de la classe"""
        pyxel.init(128, 90, title='Jeu de test')
        #définir la position du joueur
        self.pos_x = 20
        self.pos_y = 60
        #crée les liste contenant les tirs et les ennemis en cours
        self.tirs = []
        self.ennemis = []
        #crée le nombre de vie et le score
        self.vies = 3
        self.score = 0
        #lance le jeu
        pyxel.run(self.update, self.draw)
    
    def deplacement(self):
        """gérer les déplacements avec les flèches directionnelles"""
        if pyxel.btn(pyxel.KEY_RIGHT) and self.pos_x<126:
            self.pos_x += 2
        if pyxel.btn(pyxel.KEY_LEFT) and self.pos_x>0:
            self.pos_x -= 2
        if pyxel.btn(pyxel.KEY_DOWN) and self.pos_y<76:
            self.pos_y += 2
        if pyxel.btn(pyxel.KEY_UP) and self.pos_y>40:
            self.pos_y -= 2
            
    def creation_tirs(self):
        """crée un tir avec Espace"""
        if pyxel.btnr(pyxel.KEY_SPACE):
            self.tirs.append([self.pos_x+2, self.pos_y+1])
        
    def deplacement_tirs(self):
        """déplacement des tirs"""
        for tir in self.tirs:
            tir[0] += 1
            if tir[0]>128:
                self.tirs.remove(tir)
                
    def creation_ennemis(self):
        """création aléatoire des ennemis"""
        if (pyxel.frame_count % 45 == 0):
            self.ennemis.append([125, randint(40, 74)])
            
    def deplacement_ennemis(self):
        """déplacement des ennemis"""              
        for ennemi in self.ennemis:
            ennemi[0] -= 1
            if ennemi[0]<0:
                self.ennemis.remove(ennemi)
                
    def joueur_touche(self):
        """enclenche la fin du jeu si le joueur est touché"""
        for ennemi in self.ennemis:
            if ennemi[0]+2 >= self.pos_x >= ennemi[0]-1 and ennemi[1]+5 >= self.pos_y >= ennemi[1]-3:
                self.ennemis.remove(ennemi)
                self.vies -= 1

    def ennemis_touche(self):
        """disparition d'un ennemi lorsque contact avec le tir"""
        for ennemi in self.ennemis:
            for tir in self.tirs:
                if tir[1] >= ennemi[1] >= tir[1]-5 and ennemi[0] <= tir[0]+3:
                    self.ennemis.remove(ennemi)
                    self.tirs.remove(tir)
                    self.score += 1
                
    def update(self):
        """mise à jour des variables"""
        self.deplacement()
        self.creation_tirs()
        self.deplacement_tirs()
        self.creation_ennemis()
        self.deplacement_ennemis()
        
        self.joueur_touche()
        self.ennemis_touche()
        
    def draw(self):
        """création et positionnement des objets"""
        #met le fond en vert
        pyxel.cls(11)
        #crée un rectangle marron pour simuler le chemin
        pyxel.rect(0, 40, 128, 40, 9)
        #afficher le score et les vies
        pyxel.text(2, 2, f"SCORE : {self.score}", 10)
        pyxel.text(94, 2, f"VIES : {self.vies}", 12)
        #si le joueur n'est pas touché
        if self.vies > 0:
            #dessiner le joueur
            
            pyxel.rect(self.pos_x, self.pos_y, 2, 4, 4)
            #dessiner les tirs
            for tir in self.tirs:
                pyxel.rect(tir[0], tir[1], 4, 1, 10)
            #dessiner les ennemis
            for ennemi in self.ennemis:
                pyxel.rect(ennemi[0], ennemi[1], 3, 6, 8)
        else:
            #afficher GAME OVER si perdu
            pyxel.text(48, 20, 'GAME OVER', 8)
            #vider le contenu des listes d'ennemis et de tirs
            for ennemi in self.ennemis:
                self.ennemis.remove(ennemi)
            for tir in self.tirs:
                self.tirs.remove(tir)
```

[Pour tester le jeu !](https://capytale2.ac-paris.fr/web/c/6868-2465041)

Voila, maintenant que vous avec les bases, si vous voulez plus d'informations, rendez vous sur https://www.pyxelstudio.net.
Et vous être désormais libre d'exprimer votre imagination en création de jeu vidéo avec pyxel.

Arthur Hiely

Dernière modification de l'activité : Novembre 2023

Les activités partagées sur <a href="https://capytale2.ac-paris.fr/web/accueil"><strong>Capytale</strong></a> sont sous licence [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/fr/)

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
