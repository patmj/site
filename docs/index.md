# <span style="color: #E49B0F">NSI à Clemenceau</span>

Bienvenue sur le site des ressources de NSI du lycée Georges Clemenceau à Montpellier.

🚧 **EN CONSTRUCTION** 🚧

Le but de ce site est de proposer un ensemble de cours, ressources et liens pour l'enseignement de l'informatique au lycée. Il regroupe de nombreuses ressources (cours, TP, exercices, projets), dont certaines créations, mais la plupart sont des reprises ou adaptations d'activités créées et partagées par d'autres professeurs de NSI et SNT que je remercie chaleureusement 👏.

![educator](images/undraw_educator_re_ju47.svg)

Toutes les ressources, ainsi que les intentions et stratégies pédagogiques, sont partagées gratuitement sous licence CC BY-SA 4.0.