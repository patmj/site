---
title: Crédits
---

Le site est hébergé par [*la forge de des communs numériques éducatifs.*](https://forge.apps.education.fr).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

😀 Un grand merci à :
- [Vincent-Xavier Jumel](https://forge.aeif.fr/vincentxavier) et [Vincent Bouillot](https://gitlab.com/bouillotvincent) qui ont réalisé la partie technique de ce site, 

- [Mireille Coilhac](https://forge.aeif.fr/mcoilhac) créatrice de ce modèle et des mises à jour, relues par [Charles Poulmaire](https://forge.aeif.fr/cpoulmaire).

