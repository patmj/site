# <span style="color: #E49B0F">Autres plateformes d'apprentissage et sites d'exercices </span>  

| | |
|:--------------:|:-----------------:|
| <a href="https://py-rates.fr/" target="_blank"> <img img width="200" vertical-align="middle" alt="Py-rates" src="https://py-rates.fr/assets/welcomePage/logoLarge.png">  </a> **Py-rates** | <a href="https://futurecoder.forge.aeif.fr/" target="_blank"> <img width="80" alt="FuturCoder" src="https://futurecoder.forge.aeif.fr/static/logo/bordered.png">  </a> **FuturCoder**|
| <a href="https://www.codingame.com" target="_blank"> <img img width="200" alt="Codingame" src="https://upload.wikimedia.org/wikipedia/fr/thumb/1/14/CodinGame_Logo.svg/320px-CodinGame_Logo.svg.png">  </a> **Codingame** | <a href="https://www.france-ioi.org" target="_blank"> <img width="200" alt="France_ioi" src="../../images/france_ioi.svg">  </a> **France_ioi**|
| <a href="https://www.root-me.org" target="_blank"> <img width="200" alt="Rootme" src="../../images/logo_Rootme.png">  </a> **Rootme** | <a href="https://www.futureengineer.fr/" target="_blank"> <img width="200" alt="Parcours citizen code" src="https://www.futureengineer.fr/wp-content/uploads/2022/12/ctzcsaison2-min.png">  </a> **Parcours citizen code** |
| <a href="https://adventofcode.com/" target="_blank"> <img width="200" alt="Advent  of code" src="https://repository-images.githubusercontent.com/319317221/4db53000-3893-11eb-94b1-f49b2ebe46f7">  </a> **Advent  of code(en anglais)** | <a href="https://www.codepuzzle.io/" target="_blank"> <img width="200" alt="Codepuzzle" src="https://www.codepuzzle.io/img/codepuzzle-python.png">  </a> **Codepuzzle** |


## Des sites proposant des exercices pour s'entrainer (merci à [Nathalie Bessonet](https://bessonnetnathalie.forge.apps.education.fr/ressources-nsi))


### Langage Python

- [CodEx](https://codex.forge.apps.education.fr/){target="_blank"} exercices d'apprentissage de l'algorithmique et de la programmation par des professeurs d'informatique du secondaire et du supérieur

- [e-nsi ressources](https://e-nsi.forge.aeif.fr/){target="_blank"}  des professeurs d'informatique du secondaire et du supérieur, qui produisent collectivement des ressources pédagogiques sur le thème des sciences informatiques

- [Structures arborescentes](https://pratique.forge.apps.education.fr/arbre/){target="_blank"} par Franck CHAMBON : approfondir les connaissances en algorithmique et se tester en autonomie

- [Dépôt](https://forge.apps.education.fr/nreveret){target="_blank"}  de Nicolas Reveret, donnant accès à des sites pour les élèves, sur les tris, les dictionnaires, les données en tables, les bases de données... 

- [Le pyvert](https://diraison.github.io/Pyvert/){target="_blank"} site de Jean Diraison ; 128 exercices de base d'entraînement à la programmation


- [HackInScience](https://www.hackinscience.org/exercises/){target="_blank"} Exercices auto-corrigés par Julien Palard

- [GenPyExo](https://diraison.github.io/GenPyExo/){target="_blank"} Exercices auto-corrigés par Jean Diraison

 - - - - - - - - -

## Langage SQL

- [Site](https://nreveret.forge.apps.education.fr/exercices_bdd/){target="_blank"} différents exercices sur les bases de donnée et le langage SQL
par Nicolas Reveret

- [Site](https://colibri.unistra.fr/fr/course/list/notions-de-base-en-sql){target="_blank"} Notions de base en SQL  par Antoine Moulin et David Cazier

- [SQL Murder Mystery by NUKnightLab ](https://mystery.knightlab.com/){target="_blank"} et [SQL Murder](https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/12_sql/#sql-murder-mystery){target="_blank"}  version intégrée sur le site de Guillaume Connan : un meurtre a été commis dans la ville de SQL City. Il est temps d'enquêter !

- [SQL Island](https://sql-island.informatik.uni-kl.de/?lang=fr){target="_blank"} Après avoir survécu à un accident d'avion nous voilà coincé sur SQL Island ! Maintenant il faut trouver un moyen de t'échapper de cette île.

- - - - - - - 
## Bash

- [Jeux Terminus](http://luffah.xyz/bidules/Terminus/){target="_blank"} Jeu permettant de découvrir les commandes Bash.


- - - - - - - 
## Aures

- [Cube composer](https://david-peter.de/cube-composer/){target="_blank"} Puzzle avec des cubes, pour créer des formes en utilisant la programmation fonctionnelle.

- [BlockChainBattle](https://e-nsi.forge.aeif.fr/blockly-games/fr/index.html){target="_blank"} Découvrir les principes de la blockchain via un jeu vidéo.


- - - - - - -

- [PIX](https://pix.fr/enseignement-scolaire/){target="_blank"} Développer et certifier ses compétences numériques.

- [Blocky Games](https://blockly.games/?lang=fr){target="_blank"} Jeux pour les programmeurs de demain. 

- [Toxicode](http://www.toxicode.fr/learn){target="_blank"} outils ludiques pour faire développer la programmation 

- [Studio Code](https://studio.code.org/courses?lang=fr-FR){target="_blank"} Cours d'apprentissage de l'informatique pour les élèves


- [Prisonnier Quantique](https://prisonnier-quantique.fr/index.html){target="_blank"} Un jeu vidéo gratuit  pour diffuser la culture scientifique et technique.

- [CargotBot](http://www-verimag.imag.fr/~wack/CargoBot/){target="_blank"} de Rui Viana.


## Sites pour créer des jeux

- [Site Unity](https://unity.com/fr){target="_blank"}


- [Site MicroStudio](https://microstudio.dev/fr/){target="_blank"}  un moteur de jeu gratuit, en ligne, créé par Gilles Pommereuil ([Github](https://github.com/pmgl/microstudio){target="_blank"}) et [Tutoriels en Python par Teddy CHENE](https://gitlab.com/TeddyChene/microstudio){target="_blank"}


- [Godot](https://godotengine.org/){target="_blank"} Moteur de jeu

- [Renpy](https://www.renpy.org/){target="_blank"} un outil auteur permettant de créer des jeux-vidéo de type Visual novel


## Sites En Anglais

- [Codewars](https://www.codewars.com/){target="_blank"} Ameliorer ses compétences en codage en s'entrainant.

- [Python Challenge](http://www.pythonchallenge.com/about.php){target="_blank"} Un jeu dont chaque niveau peut être résolu par un petit programme.

- [RealPython](https://realpython.com/){target="_blank"} Tutoriels pour apprendre Python

- [Checkio](https://py.checkio.org/){target="_blank"} Des puzzles.

- [Robozzle](http://www.robozzle.com/beta/index.html){target="_blank"} a social puzzle game.


- [Projet Euler](https://projecteuler.net/){target="_blank"} a series of challenging mathematical/computer programming problems

- [Sphere Online Judge](https://www.spoj.com/problems/classical/sort=6){target="_blank"} Banque d'exercices

- [Hacker Rank](https://www.hackerrank.com/domains/python){target="_blank"} source d'idées d'activités 

- [Leet Code](https://leetcode.com/){target="_blank"} source d'idées d'activités 