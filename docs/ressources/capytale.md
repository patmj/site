# <span style="color: #E49B0F">Capytale</span>

Nous utiliserons la plateforme Capytale pour réaliser certaines activités et devoirs.

Voici des documentations qui pourront vous aider :

- [Utiliser des notebooks](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/eaf9-327470)
- [Wiki Capytale](https://capytale2.ac-paris.fr/wiki/)
- [Documentation de Basthon](https://basthon.fr/theme/assets/pdf/Basthon_Documentation.pdf)
- [Galerie de Basthon](https://basthon.fr/galerie.html)

👏 Merci entre autres à Nicolas Poulain pour ses carnets d'exemples, à Romain Casati et aux autres développeurs de Capytale !