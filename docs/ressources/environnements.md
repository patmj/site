# Les outils du du développeur : Environnements de travail

Auteur : Patrice MARIE-JEANNE

!!! abstract "Notions abordées"
    Terminal
    IDE
    Framework

!!! info "Références consultées"
    * Cours https://glassus.github.io/terminale_nsi/T7_Divers/1_Conseils_generaux/cours/
    * Site https://datascientest.com/ide
    * Cours git et github d'Open Classrooms
    * https://openclassrooms.com/fr/courses/6951236-mettez-en-place-votre-environnement-python
    * https://openclassrooms.com/fr/courses/5641796-adoptez-visual-studio-comme-environnement-de-developpement
    * https://python.sdv.univ-paris-diderot.fr/18_jupyter/
    * manuels consultés :
        * hachette 1ère

# Sommaire:

* [Introduction](#intro)
* [I. Utiliser la ligne de commande : le "terminal"](#chapitre1)
* [II. Utiliser un environnement de développement intégré : IDE](#chapitre2)
* [III. Script ou console ?](#chapitre3)
* [IV. Utiliser un notebook Jupyter](#chapitre4)

### Introduction <a class="anchor" id="intro"></a>

Dans cette leçon, nous allons survoler les différents outils qui permettent à un développeur ou une équipe de développeurs de mener à bien leurs projets.

Nous ne parlerons pas ici de *hardware*, plusieurs leçons du programme de NSI traitant ce thème, mais uniquement d'application logicielles. 

Les points qui suivent seront approfondis et pratiqués en cours d'année ; il s'agit ici de vous montrer ces outils et de vous inciter à **mettre en place de bonnes pratiques**. 

**Dialoguer avec la machine**

Un langage de programmation permet la transmission d'instructions et de données à un ordinateur par le moyen d'un texte qui peut être lu et compris à la fois par l'homme et la machine.

à faire : distinction texte, script, code source etc

émulateurs linux :
* https://www.cahier-nsi.fr/jslinux/ 
* https://bellard.org/jslinux/

## <span style="color: blue">I. Utiliser la ligne de commande : le "terminal" </span> <a class="anchor" id="chapitre1"></a>

!!! info "Définition"

    Un **terminal** est une interface textuelle permettant de saisir et exécuter des instructions en ligne de commande.

    (voir chapitre OS)

!!!example "Exemple 1"
    [**Terminal sous Windows**](https://learn.microsoft.com/fr-fr/windows/terminal/)
    ![image.png](attachment:image.png)
    [source](https://en.wikipedia.org/wiki/Windows_Terminal#/media/File:Windows_Terminal_v1.0_1138x624.png)

### 1.1 sous-section 1


## 2. Chapitre 2 

Les activités partagées sur ce site sont sous licence [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/fr/)

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)