# <h1 style="border: thick double #E49B0F; padding:10px; width:100%;text-align:center"> BDD 1 : Modèle relationnel </h1>   

!!! abstract "Notions introduites"

    - description et vocabulaire du modèle relationnel

    - modéliser des données dans le modèle relationnel

    - contraintes d'intégrité sur les données


# 1. Aspects historiques et motivation

## <span style="color: #E49B0F">Edgar F .Codd (1970) écrit les fondements des bases de données relationnelles</span> 

<div style="text-align: justify">  
Le père des bases de données relationnelles est <a href="https://fr.wikipedia.org/wiki/Edgar_Frank_Codd">Edgar Frank Codd</a>(1923-2003). Chercheur chez IBM à la fin des années 1960, il étudiait alors de nouvelles méthodes pour gérer de grandes quantités de données, car les modèles et les logiciels de l'époque ne le satisfaisaient pas. 
</div>
<div style="text-align: justify">  
<br/>
Mathématicien de formation, il était persuadé qu'il pourrait utiliser des branches spécifiques des mathématiques (la théorie des ensembles et la logique des prédicats du premier ordre) pour résoudre des difficultés telles que la redondance des données, l'intégrité des données ou l'indépendance de la structure de la base de données avec sa mise en œuvre physique.
</div>
<div style="text-align: justify">  
<br/>
En 1970, il publia un article où il proposait de stocker des données hétérogènes dans des tables, permettant d'établir des relations entre elles. 
    Le modèle relationnel est basé sur deux instruments puissants : l’algèbre relationnelle (c'est-à-dire le concept mathématique de relation en théorie des ensembles) et la notion de produit cartésien. Ce modèle définit une façon de représenter les données, les opérations qui peuvent être effectuées ainsi que les mécanismes pour préserver la consistance des données. E.F Codd a décrit les principes et la conception de modèle relationnel dans son livre « A relational model of data for large shared data banks ». 
</div>
<div style="text-align: justify">  
<br/>
Le scepticisme initial n'a pas empêché Codd de poursuivre ses recherches.  Ses idées ont été implémentées - plus ou moins fidèlement - dans les systèmes de gestion des bases de données relationnelles ou SGBDR telles que le projet expérimental IBM System R, puis des produits commerciaux tels qu'Oracle, DB2 ou MySQL, et dans le langage de manipulation des données SQL (1987).  
    
</div>

## <span style="color: #E49B0F">Un exemple éclairant</span> 


Nous allons imaginer un opérateur de téléphonie qui veut créer une base de données pour gérer ses clients. 

Les clients peuvent obtenir plusieurs numéros de téléphone auprès de cet opérateur. Pour chacun de ces
numéros, l'opérateur enregistrera les informations nécessaires pour établir une facture mensuelle
détaillée.

Quelles sont ces données nécessaires ?

Il faudra tout d'abord identifier de manière non équivoque le client : on enregistrera donc son
**nom**, son **prénom** et son **adresse**. 

On stockera aussi le **numéro de téléphone** qu'il a utilisé (rappelons qu'il peut en avoir plusieurs ; on fera une facture par numéro).

Il faudra aussi connaître, pour calculer les **prix** de la communication, le **numéro appelé**, la **date et
l'heure de l'appel** (pour établir la facture détaillée) ainsi que la **durée** de l'appel. Enfin, comme le prix
de l'appel peut varier selon l'heure et le numéro appelé, on mentionnera le **tarif appliqué**.

#### Une solution simple mais peu efficace: le tableur

Tableur

Cette approche a l'avantage de la simplicité mais a aussi des inconvénients :

1. Les informations sont redondantes : l'adresse du client apparaît plusieurs fois, encombrant inutilement la mémoire. Il aurait été plus judicieux d'avoir une autre table où l'on aurait enregistré les coordonnées des clients une fois pour toutes.

2. Pour identifier un client, on a besoin de trois champs. Il aurait été plus simple d'associer à chaque client un numéro de client unique (ce que toutes les entreprises font).

3. Si un utilisateur change d'adresse, il faut reporter ce changement sur chaque ligne où il apparaît, sous peine d'avoir une table incohérente.


En fait, un tableur n'est tout simplement pas fait pour gérer une grande base de données...

#### Plus efficace: une base de données relationnelle

Cette manière de faire est moins lisible au premier coup d'œil : il faut par exemple consulter deux tables pour connaître le propriétaire d'un numéro de téléphone. 

Mais cet inconvénient est vite balayé par les avantages :

1. Les informations ne sont plus redondantes : par exemple, l'adresse d'un client apparaît une seule fois et est donc facilement modifiable.

2. Un client est identifié par un numéro, ce qui évite des confusions.

3. Il est très facile de tirer la liste des clients, puisqu'ils sont tous rassemblés dans une table.

# 2. Définitions. Modèles conceptuel, logique, physique

![Du monde réel au sgbd](../../images/bdd1_image1.png)

<span style="color: red"><u>Définition 1: </u></span> 

Une <span style="color: red">base de données (BD)</span> est **un ensemble structuré d'informations**. 

Dans le langage courant, le terme peut désigner toute source importante de données telle qu'une encyclopédie. Dans le domaine de l'informatique, c'est un ensemble d'informations regroupées sous la forme d'enregistrements stockés sur un système de fichiers structurés et organisées de manière à pouvoir être facilement manipulées.

<span style="color: red"><u>Définitions 2: </u></span> 

L'organisation "abstraite" des données se fait selon un **modèle (théorique) de données**.
Une théorie de modèle de données définit généralement des niveaux de préoccupation tels que:

- **conceptuel** (par exemple, le modèle entité-association),

- **logique** (par exemple le modèle relationnel),

- **physique** (la mise en place du système d'informations avec différents outils dont les SGBD)

Le contenu des modèles pour chaque niveau peut varier selon la méthode. 

Le <span style="color: red">modèle de données relationnel</span> est aujourd'hui le plus utilisé au niveau "logique" parce qu'il permet l'indépendance entre la structure physique et l'organisation logique des données.

<span style="color: red"><u>Définition 3: </u></span> 

Le **logiciel** qui manipule les bases de données est appelé <span style="color: red">système de gestion de base de données(SGBD)</span>. Il permet d'organiser, de contrôler, de consulter et de modifier la base de données. 

<span style="color: red"><u>Définition 4: </u></span> 

Les opérations sont parfois formulées dans un **langage de requête** tel que **SQL**, qui est le plus connu et employé pour les modèles relationnels.

#### Remarque: modèle de données et structure de données

Une structure de données indique la manière dont on va organiser les données en machine tandis que le modèle indique les caractéristiques des entités modélisées et les relations entre celles-ci.

Pour un même modèle de données il peut y avoir plusieurs structures de données différentes. 

!!! info "La modélisation des données comporte plusieurs étapes"

    - la phase d'analyse dans laquelle on détermine les entités (objets, associations, personnes etc) que l'on souhaite manipuler

    - la modélisation (relationnelle dans notre cas) des entités

    - la définition des contraintes de la base de données


??? note "Phase d'analyse: <br/> Le modèle entité-association comme modèle conceptuel (Hors Programme) <br/> d'après D. Müller" 

    Dès la fin des années 1970, il est apparu que tant les modèles de SGBD (incomplets et trop techniques) que le modèle relationnel (trop pauvre) étaient inadéquats comme support de raisonnement abstrait. 
    Sont alors apparus des formalismes de description de structures de données à la fois plus abstraits que les modèles des SGBD et plus riches que le modèle relationnel : les modèles conceptuels.

    Le **modèle entité-association** s’est rapidement imposé comme modèle conceptuel.Développé dès le début des années 70, notamment en Europe [Deheneffe, 1974], et popularisé par [Chen, 1976], il propose une représentation explicite des entités du domaine d’application, de leurs associations et de leurs attributs. 

    **Exemple**

    Reprenons l'exemple de notre opérateur de téléphonie:

    1. Il y a 3 **entités** : 
        - les clients, 
        - les numéros de téléphone 
        - les appels.


    2. Les **attributs** :
        - Pour les clients : id, nom, prénom, adresse.
        - Pour les numéros de téléphone : le numéro.
        - Pour les appels : le numéro appelé, la date, la durée et le tarif.


    3. Les **relations** et les **cardinalités** :
        - Un client possède 1 ou plusieurs numéros.
        - Un numéro appartient à 0 ou 1 client.
        - Un numéro peut faire 0 ou plusieurs appels.
        - Un appel est fait depuis 1 et un seul numéro

    ![image.png](../../images/bdd1_image2.png)

    On peut classer les relations en 3 types selon leur cardinalité maximale :
        - les relations un à un (par exemple 1:1 – 0:1)

        - les relations un à plusieurs (c'est le cas des deux relations ci-dessus)

        - les relations plusieurs à plusieurs (par exemple 0:n – 1:n

??? note "Phase de modélisation: <br/>Traduction du modèle conceptuel vers le modèle (logique) relationnel (Hors Programme) <br/> d'après D. Müller"


    Les règles principales de transformation d'un schéma conceptuel entité-association en un schéma
    relationnel sont :

    **Règle I**

    Toute entité est traduite en une table relationnelle dont les caractéristiques sont les suivantes :
        - le nom de la table est le nom de l'entité ;
        - la clé de la table est l'identifiant de l'entité ;
        - les autres attributs de la table forment les autres colonnes de la table.


    **Règle II**

    Toute relation binaire plusieurs à plusieurs est traduite en une table relationnelle dont les
    caractéristiques sont les suivantes :
        - le nom de la table est le nom de la relation ;
        - la clé de la table est formée par « l'addition » des identifiants des entités participant à la relation ;
        - les attributs spécifiques de la relation forment les autres colonnes de la table.

    ![image.png](../../images/bdd1_image3.png)

    **Règle III**

    Toute relation binaire un à plusieurs est traduite :

    1. soit par un report de clé (voir schéma ci-dessous) : l'identifiant de l'entité participant à la relation côté N est ajoutée comme colonne supplémentaire à la table représentant l'autre entité. Cette colonne est parfois appelée clé étrangère. Le cas échéant, les attributs spécifiques à la relation sont eux aussi ajoutés à la même table ;

    ![image.png](../../images/bdd1_image4.png)


    2. soit par une table spécifique dont les caractéristiques sont les suivantes :
        - le nom de la table est le nom de la relation ;
        - la clé de la table est l'identifiant de l'entité participant à la relation côté 1 ;
        - les attributs spécifiques de la relation forment les autres colonnes de la table.

    **Règle IV**

    Toute relation binaire un à un est traduite, au choix, par l'une des trois solutions suivantes :
        - choix 1 : fusion des tables des entités qu'elle relie ;
        - choix 2 : report de clé d'une table dans l'autre (voir ci-dessous) ;
        - choix 3 : création d'une table spécifique reliant les clés des deux entités.

    ![image.png](../../images/bdd1_image5.png)

    Exercices (hp) dans le <a href="https://www.apprendre-en-ligne.net/info/database/BD.pdf">cours</a> de Didier Müller. 

# 3. Modèle relationnel des données: principes généraux et contraintes

Dans le modèle relationnel, les **entités** et les **associations** obtenus lors de la phase d'analyse sont transformés en tables, appelées **relations**.


<span style="color: red"><u>Définitions 5: </u></span> 

On appelle ***relation*** un tableau à deux dimensions dans lequel les ***attributs*** correspondent aux en-têtes de colonne et dont les lignes ou ***enregistrements*** sont les n-uplets des ***valeurs*** prises pour l'objet modélisé.

Exemples:

la relation Artiste

![image.png](../../images/bdd1_image6.png)

Les attributs sont : idArtiste, nom, prénom, annéeNaiss.

Un enregistrement: par exemple (1, 'Lucas', 'George', 1944)


Une relation Film

![image.png](../../images/bdd1_image7.png)

*Remarque* :

Plus formellement, une relation est un *ensemble* d'enregistrements.

Avec l'exemple ci-dessus: 

Film = {(11, 'La Guerre des étoiles', 1, 'Aventure') , (24, 'Kill Bill : Volume 1', 138, 'Action'), ..., (59, 'A History of Violence', 224, 'Drame')}

⚠
Attention à la confusion fréquente entre *relation* (ou *table*) et *entité* due au passage entre modèle conceptuel et modèle relationnel: si l'on parle bien d'entité dans le modèle conceptuel, on préférera parler de relation ou table dans le modèle relationnel, car le mot entité est ambigu, pouvant désigner une ligne/enregistrement/tuple.

<span style="color: red"><u>Définitions 6: </u></span> 

Une ***clé primaire*** est un attribut ou un ensemble d'attributs dont les valeurs permettent de distinguer les enregistrements les uns des autres. Une clé primaire peut être simple ou composée (de plusieurs attributs).

On utilise souvent un nombre entier. 

Elle est généralement indiquée par PRIMARY KEY.

Exemple: dans la relation Artiste, idArtiste est la clé primaire.


Une ***clé étrangère*** est un attribut qui est la clé primaire d'une autre relation. 
Elle est généralement indiquée par FK ("*foreign key*"), REFERENCES ou précédée d'un #. 

Exemple: dans la relation Film, idRéalisateur est une clé étrangère correspondant à la clé primaire idArtiste de la relation Artiste

<span style="color: red"><u>Définitions 7: </u></span> 

Chaque relation se conforme à un ***schéma***, c'est-à-dire une description qui indique pour chaque attribut son ***nom*** et son ***domaine***, c'est-à-dire l'ensemble des valeurs que peuvent prendre un attribut.

Exemple:

Artiste(*idArtiste* **Int**, *nom* **String**, *prénom* **String**, *annéeNaiss* **Int**) est un schéma (de relation).

<span style="color: red"><u>Définition 8: </u></span> 

Une ***base de données*** est un **ensemble de relations**.

Un ***schéma de base de données*** est l'ensemble des schémas des relations constituant la base.

Exemple:

Supposons que les deux relations ci-dessus, Artiste et Film, composent une base de données. Le schéma de la base de données est alors, présenté en ligne plutôt que comme ensemble:

Artiste(*idArtiste* **Int**, *nom* **String**, *prénom* **String**, *annéeNaiss* **Int**) 


Film(*idFilm* **Int**, *titre* **String**, *idRéalisateur* **Int**, *genre* **String**)

## <span style="color: green">Définition des contraintes d'intégrité:</span> 

Il existe un certain nombres de règles à respecter pour respecter l'intégrité d'une base de données. Ces règles visent à préserver la cohérence des données et garantir une stabilité de notre base dans le temps.

Il existe des catégories de contraintes d'intégrité à respecter :
    - contraintes d'entité
    - contraintes de domaine
    - contraintes de référence
    - contraintes d'utilisateurs


### Contraintes d'entité

Elles garantissent que chaque entité d'une relation est unique. Toute entité doit posséder une clé unique que l'on appelle clé primaire.

### Contraintes de référence

Elles créent des associations entre deux relations. Elles permettent de garantir qu'une entité d'une relation B mentionne une entité existante dans une relation A. Une clé étrangère doit faire référence à une clé primaire existante. 

### Contraintes de domaine

Elles restreignent les valeurs d'un attribut à celles du domaine et évitent que l'on puissent donner à un attribut une valeur illégale. 

Exemple : dans la relation Film ci-dessus, le genre doit être un **String**.

### Contraintes utilisateurs

Elles restreignent encore plus les valeurs d'un ou plusieurs attributs et sont guidées par la nature des données que l'on souhaite stocker dans la base.

Exemple: dans la relation Artiste, ci-dessus, l'attribut annéeNaiss ne peut être un entier quelconque 

---

## <span style="color: #E49B0F">EXERCICES</span> 

=== "Exercice 1"

    *Issu de (Balabonski et alii, 2020)*
    
    Donner la modélisation relationnelle d'un bulletin scolaire qui doit permettre de mentionner:

    - des élèves possédant un numéro d'étudiant alphanumérique unique

    - un ensemble de matières fixées, mais qui ne sont pas données

    - au plus une note sur 20, par matière et par élève

    On prendra soin de préciser toutes les contraintes utilisateurs qui ne peuvent être inscrites dans les schémas des relations 

=== "Exercice 2"

    *Issu de (Balabonski et alii, 2020)*
    
    Proposer une modélisation pour un réseau de bus qui doit permettre de générer, pour chaque arrêt de bus une fiche horaire avec tous les horaires de passage de toutes les lignes de bus qui desservent l'arrêt.

    Indication: on distinguera dans un premier temps les informations pertinentes avant de procéder à la modélisation relationnelle.

=== "Exercice 3"

    *Issu de (Balabonski et alii, 2020)*

    Donner la modélisation relationnelle d'un forum qui doit permettre de mentionner:

    - des utilisateurs du forum, qui doivent avoir a minima un id, un nom et un mot de passe

    - des sujets de discussions

    - des messages, qu'on veut pouvoir organiser chronologiquement et relier à un sujet de discussion et un auteur

    On prendra soin de préciser toutes les contraintes utilisateurs qui ne peuvent être inscrites dans les schémas des relations

----

Références:

- le <a href="https://www.apprendre-en-ligne.net/info/database/BD.pdf">cours</a> de Didier Müller;
- les articles wikipedia: <a href="https://fr.wikipedia.org/wiki/Mod%C3%A8le_relationnel"> modèle relationnel </a> et <a href="https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es"> base de données</a>;
- le <a href="http://www.monlyceenumerique.fr/nsi_terminale/bdd/bdd1.php"> cours </a> de "Mon lycée numérique";
- BALABONSKI et alii, Numérique et Sciences Informatiques Terminale, Ellipses, 2020;
- HAINAUT J-L, "Base de données et modèles de calcul", Dunod, 2005 

---

<a href="https://forge.apps.education.fr/patmj" target="_blank">Patrice MARIE-JEANNE</a>
  &copy; 2023 sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank">CC BY-SA 4.0</a> 
  <img src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" alt="logo CC BY-SA 4.0" align="right">
  <br> Illustrations par <a href="https://undraw.co/" target="_blank">UnDraw</a>