# <span style="color: #E49B0F">Bases de données</span> 

!!! abstract "Notions abordées"
    * [Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.](./bdd1_modele_relationnel.md)
    * [Base de données relationnelle.]()
    * [Système de gestion de bases de données relationnelles.]()
    * [Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données.]()

## Carte mentale 

## Évaluation 

critères d'évaluations etc