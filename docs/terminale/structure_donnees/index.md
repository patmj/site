# <span style="color: #E49B0F">Structures de données</span> 

!!! abstract "Notions abordées"
    * [Structures de données, interface et implémentation.]()
    * [Vocabulaire de la programmation objet : classes, attributs, méthodes, objets.]()
    * [Listes, piles, files : structures linéaires. Dictionnaires, index et clé.]()
    * [Arbres : structures hiérarchiques. Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits.]()
    * [Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés.]()

## Carte mentale 

## Évaluation 

critères d'évaluations etc