# <span style="color: #E49B0F">Algorithmique</span>

!!! abstract "Notions abordées"
    * [Algorithmes sur les arbres binaires et sur les arbres binaires de recherche.]()
    * [Algorithmes sur les graphes.]()
    * [Méthode « diviser pour régner ».]()
    * [Programmation dynamique.]()
    * [Recherche textuelle.]()  

## Carte mentale 

## Évaluation 

critères d'évaluations etc