# <span style="color: #E49B0F">Langages et programmation</span> 

!!! abstract "Notions abordées"
    * [Notion de programme en tant que donnée. Calculabilité, décidabilité.]()
    * [Récursivité.]()
    * [Modularité.]()
    * [Paradigmes de programmation.]()
    * [Mise au point des programmes. Gestion des bugs.](map_bug/index.md)

## Carte mentale 

## Évaluation 

critères d'évaluations etc