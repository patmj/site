<h1 style="border: thick double orange; padding:10px; width:100%;text-align:center">
Mise au point des programmes
    <br/> Spécifier, réaliser, documenter et tester
</h1> 

!!! abstract "Notions abordées"
    - pseudo-code
    - spécification : pré-conditions, post-conditions
    - réalisation : (in)variant de boucle, correction et terminaison d'un programme
    - documentation et commentaires
    - erreurs, tests, debug
    - utilisation de bibliothèques


!!! info "Références"
    - Le <a href="https://www.apprendre-en-ligne.net/info/structures/structures.pdf">cours</a> de Didier Müller.

    - Le <a href="http://frederic-junier.org/NSI/premiere/chapitre3/">cours</a> de Frédéric Junier.

    - BALABONSKI et alii, Numérique et Sciences Informatiques Terminale, Ellipses, 2020

    - BEAUDOUIN et alii, Numérique et Sciences Informatiques Première, Hachette, 2021


# <span style="color: red"><u>I. Spécification et réalisation </u></span> 

## <span style="color: blue"><u>I.1. Utilisation du pseudo-code pour spécifier un programme </u></span> 

<span style="color:red; font-weight: bold;">Définitions 1</span>

Le <span style="color:#9400D3; font-weight: bold;">pseudo-code</span> est un langage proche du langage naturel qui permet de décrire un algorithme sans référence à un langage de programmation en particulier. 

Cela permet de développer une démarche structurée de construction du programme sans rentrer dans les détails d'un langage de programmation comme Python. 

Il n'existe pas de pseudo-code standard.

Les descriptions du pseudo-code peuvent être précisées durant le développement du projet: structures de données, types, fonctions ... 
On parle de **raffinement**.


A l'issue du raffinement, on doit disposer d'un ensemble de <span style="color:#9400D3; font-weight: bold;">spécifications</span> (ou **prototype**) des scripts et fonctions : 

 - pseudo-code; 
 - nature, type et domaine des paramètres avec éventuellement des **pré-conditions**;
 - valeur(s) de retour éventuelle(s);
 - **post-condition(s)** éventuelle(s); 
 - plus généralement **assertions** à verifier dans certains endroits du programme, comme les **(in)variants de boucle**

!!! Example "Exemple de Pseudo code"

    |Fonction factorielle (n : entier positif)|
    |:-----------------------|
    |*pré-condition: n est un entier positif (on peut l'omettre si le langage est typé)*|
    |*post-condition : la sortie r est la factorielle de n*|
    |r = 1                   |
    |Pour i de 1 jusqu'à n avec un pas de 1|
    |r = r*i                               |
    |*assertion r = factorielle de i = i!*  |
    |Fin pour                              |
    |Retourner r                           |
    |Fin Fonction                          |


## <span style="color: blue"><u>I.2. Réaliser le programme </u></span> 

Une fois le programme spécifié, on peut entamer sa **traduction du pseudo-code vers un langage de programmation** comme Python.

Selon le langage de programmation, on peut perdre certaines informations au moment de cette traduction ou encore être amené à modifier certaines données pour les représenter dans un type du langage. 

En Python, qui est un langage non typé, il peut être souhaitable de vérifier le type des arguments en tant que **pré-conditions**. On peut également vérifier systématiquement les différentes **assertions** et les **post-conditions**.
On parle alors de **programmation défensive**.

!!! abstract "Définition"

    Une **assertion** est une instruction qui vérifie si une condition (à valeur booléenne) est vérifiée dans l’état
    courant du programme.

    L’exécution d’une assertion est silencieuse si elle est vérifiée et elle lève une exception de type `AssertionError`
    qui interrompt l’exécution sinon.

    La syntaxe est `assert condition`.

!!! example "Exemples"

    ```python
    #un script effectuant un calcul uniquement si l'entrée est un nombre positif
    age = float(input("Quel est ton âge ?"))
    assert age > 0, 'Ton age doit être positif !'
    print(f"Tu as {age} ans !")
    ```


    ```python
    def factorielle(n: int):
        assert isinstance(n, int), 'n doit être entier' 
        assert n >= 0, 'n doit être positif'
        r = 1
        for i in range(1, n+1):
            r = r * i
        return r
    ```

# <span style="color: red"><u>II. Documentation et commentaires </u></span> 

Pour les projets complexes ou de grande taille, à destination de ou faisant intervenir de nombreuses personnes ou étalé dans le temps, il est important de **documenter** et **commenter** ses programmes.

En Python, on peut réaliser cela:

- grâce aux **commentaires** introduits par # dans le code. Ce genre de documentation est destinée aux développeurs. On doit veiller à apporter une information qui n'est pas une simpe paraphrase du code 


- en associant une chaîne de caractères appelée ***docstring*** lors de la définition d'une **fonction**. Elle est introduite par trois guillemets au début de la ligne suivant la définition de la fonction. Une bonne *docstring* doit donner une description de la fonction, de ses paramètres et éventuelle(s) valeurs de retour. On peut également spécifier la méthode employée. La fonction `help(foo)` permet d'obtenir la *docstring* de `foo`.


```python
#un exemple de docstring, et de commentaire...
def factorielle(n: int)-> int:
    """Retourne la factorielle de l'entier positif n
    Entrée : n entier, n >= 0
    Sortie : factorielle n, entier >= 0
    Le calcul est effectué au moyen d'une boucle Pour.
    """
    assert isinstance(n, int), 'n doit être entier' 
    assert n >= 0, 'n doit être positif'
    r = 1
    for i in range(1, n+1):
        r = r * i
    return r
```


```python
help(factorielle)
```

En ce qui concerne les noms des fonctions et variables employés, il faut se référer à la <a href="https://peps.python.org/pep-0008/">PEP8</a>. De façon générale, privilégier le confort de (re)lecture à celui d'écriture.

# <span style="color: red"><u>III. Tests </u></span> 

Tout programme doit être **testé**.

!!! quote "Citation" 
    [E. W. Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) :

    | | |
    |:-------------------------:|:-------------------------:|
    |<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Edsger_Wybe_Dijkstra.jpg/800px-Edsger_Wybe_Dijkstra.jpg" alt="Edsger Wybe Dijkstra" width="200" height="200" />|<blockquote> <div>Testing shows the presence, not the absence of bugs</div></blockquote>|

    soit « Tester un programme démontre la présence de bugs, pas leur absence. »
    

A défaut de pouvoir effectuer tous les tests possibles, on devra construire des jeux de tests qui couvrent le mieux possible les différents cas d'usages de nos fonctions :

 - cas typiques : avec des données d'entrées "normales";
 
 - cas limites : les données respectant de façon limite les pré-conditions (bornes de définition, 0, tableau vide...)
 
 - les cas interdits pour s'assurer que la fonction va bien les rejeter et renvoyer un message d'erreur.

En réalité, il est même préférable de **commencer par écrire le jeu de test** puis la fonction à tester.

On parle alors de *Test-Driven Development (TDD)*, ou développement piloté par les tests, qui permet généralement d'obtenir un code de meilleure qualité.


```python
#ces jeux de test auraient pu/dû être écrit avant la fonction elle-même
def test_factorielle_1():
    assert factorielle(0) == 1
    assert factorielle(1) == 1
    assert factorielle(4) == 24

def test_factorielle_2():
    """teste que factorielle lève bien une erreur pour une valeur négative"""
    try:
        factorielle(-4) # ok si erreur
    except:
        return
    assert False, "factorielle devrait provoquer une erreur"
    

def test_factorielle_3():
    """teste que factorielle lève bien une erreur pour un float"""
    try:
        factorielle(2.5) # ok si erreur
    except:
        return
    assert False, "factorielle devrait provoquer une erreur"

test_factorielle_1()
test_factorielle_2()
test_factorielle_3()
```

En Python, il existe des outils comme `pytest`, `unittest` ou `doctest` qui permettent de faciliter les tests.

La forme du jeu de test dépend de l'outil choisi.


```python
def factorielle(n: int)-> int:
    """Retourne la factorielle de l'entier positif n
    Entrée : n entier, n >= 0
    Sortie : factorielle n, entier >= 0
    Le calcul est effectué au moyen d'une boucle Pour.
    
    >>> factorielle(0)
    1
    >>> factorielle(1)
    1
    >>> factorielle(4)
    24
    >>> factorielle(-4)
    Traceback (most recent call last):
    ...
    AssertionError: n doit être positif
    >>> factorielle(2.5)
    Traceback (most recent call last):
    ...
    AssertionError: n doit être entier
    """
    assert isinstance(n, int), 'n doit être entier' 
    assert n >= 0, 'n doit être positif'
    r = 1
    for i in range(1, n+1):
        r = r * i
    return r

import doctest
doctest.testmod()
```

<div class="alert alert-block alert-success"> 
    <h1> Exercices ✯ </h1>
    <br/>
</div>   

On cherche à construire un programme nommé `pgcd` calculant le PGCD de deux nombres entiers.

1. Écrire un ensemble de tests comparant le résultat de cette fonction `pgcd` avec la fonction `gcd` du module `math`


```python
from math import gcd

gcd(18, 15)
```

```python
# à faire
```

2. Écrire un test destiné à vérifier la levée d'une exception par votre fonction `pgcd`.


```python
# à faire
```

3. Écrire à l'aide de `assert` des pré-conditions destinées à détecter les cas d'erreurs pour les arguments passés à votre fonction `pgcd`.


```python
# à faire
```

4. Écrire votre fonction, avec ces pré-conditions,  et tester la sur les ensembles de tests construits dans les questions précédentes.


```python
# à faire
```

<i>Patrice MARIE-JEANNE </i><br>

<span style="font-size:10px">Dernière modification de l'activité : Septembre 2024</span>
![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

