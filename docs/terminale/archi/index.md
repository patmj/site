# <span style="color: #E49B0F">Architectures matérielles, systèmes d’exploitation et réseaux</span> 

!!! abstract "Notions abordées"
    * [Composants intégrés d’un système sur puce.]()
    * [Gestion des processus et des ressources par un système d’exploitation.]()
    * [Protocoles de routage.]()
    * [Sécurisation des communications.]()

## Carte mentale 

## Évaluation 

critères d'évaluations etc