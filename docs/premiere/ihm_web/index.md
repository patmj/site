# <span style="color: #E49B0F">Interactions entre l'homme et la machine sur le web</span>

!!! abstract "Notions abordées"
    * [Modalités de l’interaction entre l’homme et la machine. Événements]()
    * [Interaction avec l’utilisateur dans une page Web]()
    * [Interaction client-serveur. Requêtes HTTP, réponses du serveur]()
    * [Formulaire d’une page Web]()

## Carte mentale 

## Évaluation 

critères d'évaluations etc
