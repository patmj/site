# <span style="color: #E49B0F">Architectures matérielles et systèmes d'exploitation</span>

!!! abstract "Notions abordées"
    * [Modèle d’architecture séquentielle (von Neumann)](https://capytale2.ac-paris.fr/web/c/ce39-1233273)
    * [Transmission de données dans un réseau. Protocoles de communication. Architecture d’un réseau]()
    * [Systèmes d’exploitation]()
    * [Périphériques d’entrée et de sortie. Interface Homme-Machine (IHM)]()

## Carte mentale 

## Évaluation 

critères d'évaluations etc