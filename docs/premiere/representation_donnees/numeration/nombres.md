<h1 style="border: thick double orange; padding:10px; width:100%;text-align:center;font-size:220%;"> Nombres et systèmes de numération</h1>

!!! abstract "Notions abordées"
    * Systèmes de numération : base, notation, énonciation
    * Histoire des systèmes de numération
    * Système décimal positionnel



!!! info "Références"
    * Wikipédia: [Systèmes de numération](https://fr.wikipedia.org/wiki/Système_de_numération), [Numération](https://fr.wikipedia.org/wiki/Numération)
    * Cours sur [lelivrescolaire](https://www.lelivrescolaire.fr/page/6870830)
 

# <span style="color: green">Sommaire</span> <a class="anchor" id="sommaire"></a>

## [Introduction](#intro)


## [I. Nombres et systèmes de numération.](#chapitre1)   
   * [I.1 Quelques repère historiques](#section_1_1)
   * [I.2 Base(s) et chiffres](#section_1_2)
   * [I.3 Inventer des nombres](#section_1_3)
   
   
## [II. Systèmes de numérations additifs](#chapitre2)
   * [II.1 Un système de numération égyptien](#section_2_1)
   * [II.2 Additifs ... mais pas seulement](#section_2_2)

## [III. Systèmes de numération hybrides](#chapitre3)
   * [III.1 Une numération chinoise traditionnelle](#section_3_1)
   * [III.2 La numération orale française ! ](#section_3_2)

## [IV. Les systèmes positionnels](#chapitre4)
   * [IV.1 Les premiers systèmes positionnels](#section_4_1)
   * [IV.2 Le système positionnel décimal indo-arabe](#section_4_2)

## [Conclusion](#conclusion)

[Ressources supplémentaires](#ressource)


---


### <span style="color: green">Introduction</span> <a class="anchor" id="intro"></a>

L’histoire des nombres est intimement liée à celle du **dénombrement** (nombre « nombrant », **numération**) et de la **mesure** (nombre « nombré », **métrologie**), et donc à l’histoire des techniques, théories, outils et récits que les différentes civilisations se sont donnés pour décrire le monde. 

Nous aborderons essentiellement la numération dans cette leçon, mais il ne faut pas perdre de vue qu'un système de numération et son usage est lié à la métrologie de son contexte d'utilisation.

La numération est l’ensemble des règles qui permettent de **nommer**, d’**écrire** ou de **mimer** les nombres.

On peut ainsi distinguer les sous-familles suivantes parmi les systèmes de numération:
* les **systèmes d'énonciation**, pour dire les nombres. 
* les **systèmes de notation**, pour écrire les nombres.
* les **systèmes de mime**, pour transmettre une information numérique par la gestuelle: un [exemple](https://www.youtube.com/watch?v=H13Se4nBPDA).

Les nombres ont de [multiples usages](https://fr.wikipedia.org/wiki/Numération#Applications)...

# <span style="color: red"><u>I. Nombres et systèmes de numération</u></span> <a class="anchor" id="chapitre1"></a>

## <span style="color: blue">I.1 Quelques repère historiques</span> <a class="anchor" id="section_1_1"></a>

Nous allons aborder quelques passages clefs dans l’histoire des nombres et des systèmes de numération pour comprendre leur formalisation actuelle en mathématiques.

[Frise](https://capytale2.ac-paris.fr/web/sites/default/files/2023/06-30/23-21-41/Timeline.html)

Une série de vidéos intéressante sur les nombres, par Mathador :

[Le chiffre 1](https://www.youtube.com/watch?v=O0pKb3-YTzU)

[Le chiffre 0](https://www.youtube.com/watch?v=qU24Zvs3mZM)

### Archéologie mathématique

Les plus anciennes marques numériques d'*homo sapiens* datent des premières civilisation du Paléolithique (40 000 / 30 000 ans environ av. J.-C.). 

On a ainsi retrouvé ce qui semblent être des **bâtons de comptage** (ou bâton de taille) :  ce sont probablement des systèmes mnémoniques destinés à enregistrer un nombre grâce à des marques de dénombrement portées sur un bâton. Le plus souvent, ces marques sont des entailles, sur un bâton en bois ou en os.

![](https://tangente-mag.com/img/TG190_28_img1.jpg)

FIG.1 - Os de Lebombo, trouvé dans une caverne à la frontière de l’Afrique du Sud et du Swaziland en 1970, daté d’environ 43 000 ans avant notre ère. Issu d’un péroné de babouin, [*magazine Tangente*](https://tangente-mag.com)

Vers le VIIIème millénaire av. J-C., les *calculi*, des jetons de comptage apparaissent en **Mésopotamie** - où l'on trouve également des carrés et cercles dès le VI millénaire av. J-C.

Plus tard, vers 3400-3100 avant J.-C., ces calculi sont placés dans des bulle-enveloppes en argile légèrement cuite afin de sécuriser les transactions commerciales.

![](https://www.louvrelens.fr/wp-content/uploads/2020/11/Bulle-calculi-%C2%A9-RMN-GP-musee-du-Louvre-Gerard-Blot-800x577.jpg)

FIG.2 - Bulle-enveloppe et son contenu de jetons (calculi), Suse, Iran actuel vers 3400-3100 avant J.-C., argile légèrement cuite © RMN-GP (musée du Louvre) Gérard Blot

### Les premières traces d'écriture numérique

Les symboles représentant des nombres se trouvent dans les premières traces d’écriture de l’humanité, **avant même les écrits « littéraires »**. Ce sont des petits signes en cunéiforme à l’extérieur des « bulles » en argile contenant des jetons de comptage, qu’il fallait auparavant briser pour obtenir le décompte.

Ainsi, les plus anciens écrits sumériens sont des « livres de comptabilité » qui ont été retrouvés dans les ruines des temples d’Uruk et Lagash en Mésopotamie et sont datées de 3 300 avant J-C (les premiers textes littéraires, racontant l’épopée de Gilgamesh, datant de 2 600 av J-C). 

![](https://images.math.cnrs.fr/IMG/png/chambon_fig1bis.png)

FIG.3 - Carte du Proche-Orient ancien (ici époque paléobabylonienne, 1900-1600 av. J.-C.), [Images des Maths](https://images.math.cnrs.fr/Les-grands-nombres-innombrables-en-Mesopotamie.html)

---

En **Chine**, quelques découvertes archéologiques permettent de conjecturer l'apparition des nombres écrits dès le IIIème millénaire av. J-C., mais on ne dispose de rien de comparable à ce qu'on connait des mathématiques babyloniennes ou égyptiennes (tablettes, papyri, etc.).

Les plus anciens documents retrouvés sont des calculs simples en écriture ossécaille ; ils remontent à la dynastie Shang (1600–1050 av. J.-C.). Le Yi Jing est le plus ancien ouvrage ayant survécu qui ait un contenu mathématique.
(source [Wikipédia](https://fr.wikipedia.org/wiki/Math%C3%A9matiques_chinoises))

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Beijing.printing.museum-Henan.Anyang.Jiaguwen.jpg/1280px-Beijing.printing.museum-Henan.Anyang.Jiaguwen.jpg)

FIG.4 - Écriture ossécaille (Wikipedia)

---

La civilisation méso-américaine des Mayas a elle aussi développé un système de numération écrite savante dont l'existence est attestée par les trois (!!) documents qui ont échappé à la destruction :

|  |  |  |
|:-|:--:|---:|
|![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Dresden_codex%2C_page_2.jpg/800px-Dresden_codex%2C_page_2.jpg)|FIG. 5 - Le codex de Dresde (Wikipedia) | |
|![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Paris_Codex%2C_pages_23-24.jpg/1024px-Paris_Codex%2C_pages_23-24.jpg) | FIG. 6 - Le codex de Paris (Wikipedia)| |
|![](https://upload.wikimedia.org/wikipedia/commons/7/7b/Codex_Tro-Cortesianus.jpg) | FIG. 7 - Le codex de Madrid (Wikipedia) | |

Quelques points intéressants : 

 * les Mayas distinguaient l'aspect cardinal - la quantité 3 par exemple - de l'aspect ordinal du nombre - le troisième d'une course ; ils ne confondaient pas une durée avec une date. 
 
 * leur étrange système d'énonciation des nombres utilisait la [protraction](https://fr.wikipedia.org/wiki/Protraction_(num%C3%A9ration)) : 55 était ainsi le "15 de la troisième vingtaine". 

* enfin, et surtout, il semble qu'ils utilisaient dans un certain contexte (le [compte long](https://fr.wikipedia.org/wiki/Compte_long)) une [numération de position](#chapitre4) à étages qui leur permettait de calculer des dates précises dans ce calendrier qui, partant de la création du monde selon les Mayas, permettait de représenter les "grands cycles" de création d'univers (tous les 5 125 ans environ).

## <span style="color: blue">I.2 Base(s) et chiffres</span> <a class="anchor" id="section_1_2"></a>

L'usage de **base** pour simplifier la manipulation des nombres est attestée dès l'apparition de l'écriture, et lui préexiste probablement.

Avec une base, on compte **"par paquets"** au lieu de compter uniquement par unités. Cela simplifie grandement l'écriture et l'énonciation des nombres.

La base la plus fréquente dans la numération quotidienne est la **base décimale (10)**, mais on trouve aussi souvent: 

* des bases sexagésimale (60), utilisée par les Sumériens entre autres, 
* vicésimale (20), utilisée par les Mayas,
* duodécimale (12),
* quinaire (5), utilisée aussi par les Mayas
* binaire (2).

[Atlas des bases numériques](https://wals.info/feature/131A#2/26.7/146.3)

!!! note "Une coexistence possible:"
 
    Dès les prémices de l’écriture cunéiforme sur les tablettes d’argile à la fin du IVe millénaire av. J.-C., deux bases numériques ont coexisté dans les textes administratifs et les inscriptions royales : une base décimale (fondée sur le nombre 10) et une base sexagésimale (fondée sur le nombre 60).

    Ces bases et les systèmes de numération les utilisant se sont influencés, mélangés et métissés dans cette région pendant des milliers d'années.  

    Pour en [savoir plus](https://images.math.cnrs.fr/Les-grands-nombres-innombrables-en-Mesopotamie.html)
 


Plusieurs bases/"paquets" peuvent coexister dans le **même** système de numération, comme le montre les systèmes mésopotamiens ou mayas par exemple (voir plus bas).

!!! danger "Base, définition mathématique"

    En mathématiques, et plus précisément en arithmétique, on parle de [base](https://fr.wikipedia.org/wiki/Base_(arithmétique)) d'un système de numération lorsque les seuls "paquets" autorisés sont les puissances successives de cette base. 

    En revanche, on parle  de bases *irrégulières* ou *mixtes* lorsque le système de numération dispose de plusieurs bases. 

Exemples : 

* notre système actuel dispose d'une base 10 ; les paquets sont ceux que vous avez appris à l'école élémentaire : unité, dizaine (10 unités), centaines (10 dizaines) etc. Les chiffres sont 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.

* Les Mayas disposaient eux aussi d'un système positionnel irrégulier en base 20, utilisé pour le "compte long" :

![image.png](attachment:image.png)

$ 9 018 = 1 \times 7200 + 5 \times 360 + 0 \times20  + 18 $

Les bases/paquets sont constitués ainsi :

    - le kin (jour) est l'unité de base, les Mayas n’ayant pas d’équivalent à notre semaine ;
    - le winal, plus ou moins équivalent à notre mois, qui comporte 20 jours ;
    - le **tun qui correspond, non pas à 20 × 20, mais à 18 × 20 = 360 jours**. Cela fait ainsi apparaitre une **irrégularité** dans le système vigésimal. Cette unité est proche de l’année solaire. À partir du tun le système est absolument vigésimal ;
    - le katun, c'est-à-dire 20 tuns ou « années » ;
    - le baktun, c'est-à-dire 20 katuns (= 400 tuns ou « années »).
    - Le système se prolonge au delà du baktun avec le pictun (20 baktuns ou 8 000 tuns), le kalabtun (20 pictuns ou 160 000 tuns), le kinchiltun (20 kalabtuns ou 3 200 000 tuns), l'alautun (20 kinchiltun soit 64 000 000 tuns ou « années » de 360 jours). 

À cause de cette irrégularité présente entre le deuxième chiffre et le troisième, **la multiplication par la base ne peut pas se réduire à un simple décalage des chiffres vers le haut**.

* à l'inverse, le système décimal chinois de ["baguettes à calculer"](https://fr.wikipedia.org/wiki/Baguettes_%C3%A0_calculer) permettait d'effectuer des calculs de manière efficace, malgré quelques ambiguïtés.

source: [Wikipedia](https://fr.wikipedia.org/wiki/Notation_positionnelle)

!!! danger "Écriture dans une base"

    Dans le cas d'une base "régulière", tout nombre entier *n* peut se décomposer sous la forme:

      $n = a_{0} + a_{1} \times b + a_{2} \times b^{2} + ... + a_{N} \times b^{N} $, où les $ a_{k} $ sont des nombres entiers inférieurs à b et $ a_{N} \neq 0 $

   On peut ainsi écrire le nombre *n* dans la base *b* grâce au [principe positionnel](#chapitre4): 

      $ n = {a_{N}a_{N-1}...a_{0}}_{b}$

   Par exemple: 

      $ 5 + 2 \times 10 + 1 \times 10^{2} = 125_{10} $ écriture dans laquelle on omet généralement le 10 en indice ;

      mais on a également $ 125_{10} = 1 \times 5^{3} = 1000_{5} $ 



## <span style="color: blue">I.3 Inventer des nombres</span> <a class="anchor" id="section_1_3"></a>

Tous les systèmes de numération évoqués ne permettent de représenter que certains types de nombres : entiers, décimaux, fractions...

**Cependant, des nouveaux nombres ou systèmes de nombres apparaissent régulièrement, le plus souvent en réponse à de nouvelles percées techniques ou scientifiques.**

Il a donc fallu réfléchir au concept même de nombre pour pouvoir quantifier ou ordonner les "grandeurs" nouvellement créées.

Un schéma ... incomplet:

|  |  |  |
|:-|:--:|---:|
|  | ![](attachment:image.png)  |   |
|  | FIG. 8 - *Les ensembles de nombres en seconde, [lelivrescolaire](https://www.lelivrescolaire.fr/page/6870864)*  |  |


Le schéma précédent **n'est pas** le "mot de la fin" en ce qui concerne les nombres. N'y apparaissent pas : les complexes, quaternions, octonions, les nombres p-adiques, transfinis etc.

!!! tip "Analyse des systèmes de numération"
    Outre sa commodité d'énonciation ou d'écriture, un système de numération sera jugée "bon" - du point de vue mathématique - lorsqu'il permet d'effectuer facilement certaines opérations/transformations.

    Plus largement, les familles de nombres, les systèmes de numération et les systèmes métrologiques doivent être envisagés comme des outils pour effectuer du dénombrement élémentaire, exprimer des mesures physiques, résoudre des équations, appréhender l’infini. 



# <span style="color: red"><u>II. Systèmes de numération additifs</u></span> <a class="anchor" id="chapitre2"></a>

Ce sont les plus anciens systèmes connus. Bien qu'intuitifs, ils sont peu commodes à utiliser pour représenter des grands nombres ou effectuer des opérations élémentaires (multiplication etc).


!!! danger "Systèmes additifs"
    Dans les systèmes de numération additifs, **chaque symbole a sa propre valeur, qui est indépendante de sa position  dans la représentation du nombre**. 

    Il faut **additionner** les valeurs de chaque symbole pour trouver la valeur numérique représentée.


## <span style="color: blue">II.1 Un système de numération égyptien (de 3000 avant J.-C. à 300 avant J.-C)</span> <a class="anchor" id="section_2_1"></a>

Il y avait 7 symboles différents représentant chacun une puissance de dix.

![image.png](attachment:image.png)
FIG. 9 - Puissances de dix dans une numération égyptienne

!!! example "Exemple d'écriture dans le système de numération égyptien"
 
    ![image.png](attachment:image.png)
 

On peut ainsi écrire les nombres jusqu'à 999 999 999.

On trouve aussi une écriture des fractions (uniquement du type 1/n) sur le papyrus Rhind (roi Narmer).

![image.png](attachment:image.png)

FIG.10 - *Détail d'une des deux principales parties du papyrus Rhind, British Museum, EA 10057, [Wikipédia](https://fr.wikipedia.org/wiki/Papyrus_Rhind#/media/Fichier:Rhind_Mathematical_Papyrus.jpg)*




Le hiéroglyphe en forme de bouche ouverte qui signifie partie, était utilisé pour représenter le numérateur 1 : 

![image.png](attachment:image.png)


Les fractions étaient écrites avec ce hiéroglyphe dessus et le dénominateur en dessous. Ainsi 1/3 était écrit :

![image.png](attachment:image.png)


Il y avait des symboles spéciaux pour les fractions les plus courantes comme 1/2 et pour deux fractions non unitaires 2/3 et 3/4 :

![image.png](attachment:image.png)

Si le dénominateur devenait trop large, la « bouche » était placée juste au début du dénominateur :

![image.png](attachment:image.png)


*[Wikipédia](https://fr.wikipedia.org/wiki/Fraction_%C3%A9gyptienne)*

!!! note "Remarque"
    Attention, comme dans de nombreux domaines, nous avons une tendance à surinterpréter les symboles utilisés dans ce que nous pensons être des contextes mathématiques.
    Exemple probable de surinterprétation : [oeil oudjat](https://fr.wikipedia.org/wiki/%C5%92il_oudjat#Interpr%C3%A9tation_m%C3%A9trologique)



## <span style="color: blue">II.2 Additifs ... mais pas seulement </span> <a class="anchor" id="section_2_2"></a>

Contrairement à l'exemple précédent, de nombreux systèmes additifs sont constitués d'autres ingrédients: 

* le système de numération mésopotamien mêle principe additionnel et [principe positionnel](#section_4_1)

* le système romain utilise l'ordre des chiffres sans être positionnel: CX signifie cent plus dix alors que XC signifie cent moins dix. 

# <span style="color: red"><u>III. Systèmes de numération hybrides</u></span> <a class="anchor" id="chapitre3"></a>

!!! danger "Systèmes hybrides" 
    Dans les **numérations hybrides**, on utilise à la fois **l’addition et la multiplication** pour désigner les nombres.

    Ces systèmes utilisent des chiffres pour les unités et pour les puissances de la base. Les chiffres représentant une puissance de la base utilisés sont, au besoin, combinés avec un chiffre représentant une unité, et les nombres sont ainsi représentés par addition de multiples de puissances de la base. 


C'est le cas des systèmes de numération chinois et japonais traditionnels, par exemple.

Comme les systèmes additifs, certaines numérations hybrides sont également positionnelles.

## <span style="color: blue">III.1 Une numération chinoise traditionnelle</span> <a class="anchor" id="section_3_1"></a>

La numération chinoise, présentée [ici](https://fr.wikipedia.org/wiki/Numération_chinoise), utilise 13 symboles différents :

![image.png](attachment:image.png)
FIG. 11 - Les symboles d'une numération chinoise traditionnelle

On remarquera la présence d'un zéro, dont l'apparition dans le système est tardive (XIIIème siècle)

!!! example "Exemple d'écriture dans un système de numération chinois hybride"
 
    ![image.png](attachment:image.png)
 


Les systèmes hybrides représentent une économie de symboles dans l’écriture des nombres par rapport aux systèmes additifs. On peut remarquer qu'un tel système de notation comporte une forte analogie avec le système d'énonciation des nombres dans une majorité de langues. 


!!! warning "Limitations"

    En revanche, pour chaque nouvelle position ou groupement exprimé, un nouveau signe ou symbole s’impose ce qui rend évident **la faiblesse de ce principe pour représenter des très grands nombres.**


## <span style="color: blue">III.2 La numération orale française !</span> <a class="anchor" id="section_2_1"></a>

Comment lisez-vous le nombre « 280 » ?

Arrivez-vous à lire en français le nombre « 180 532 527 384 423 122 » ? Indice : [ici](https://fr.wikipedia.org/wiki/Noms_des_grands_nombres) ou encore [ici](https://fr.vikidia.org/wiki/Grand_nombre)

# <span style="color: red"><u>IV. Les systèmes positionnels</u></span> <a class="anchor" id="chapitre4"></a>

!!! danger "Principe positionnel"

    Dans un **système à notation positionnelle**, la position des chiffres ou symboles dans le nombre détermine leur valeur.

    Par exemple: le système maya du "compte long", [déjà mentionné](#section_1_2), est un système positionnel à base irrégulière.

    En particulier, dans un **système de numération avec base régulière**, chaque position est reliée à la position "voisine" par la multiplication par la base.

    Ainsi, par exemple: 

    $ 34 = 3 \times 10 + 4 $ ce qui diffère de $ 43 = 4 \times 10 + 3 $ 


## <span style="color: blue">IV.1 Les premiers systèmes positionnels</span> <a class="anchor" id="section_2_1"></a>

Provenant de la numération sumérienne, le système de numération mésopotamien est partagée par les Babyloniens, les Akkadiens et d’autres peuples à partir du IIIème millénaire avant J-C jusqu’au IIIème siècle avant J-C.

C’est un compromis entre la base 10 et la base 60 dont il reste des vestiges dans le système horaire et dans la mesure des angles.                                                                           

Plusieurs systèmes de numération utilisant l’écriture cunéiforme ont en fait cohabité, mais l’usage du système positionnel de l'exemple ci-dessous semble avoir été limité au domaine savant.

![image.png](attachment:image.png)

FIG.12 - *Tablette YBC 7289 et sa traduction en numération actuelle, [lelivrescolaire](https://www.lelivrescolaire.fr/page/6870830)*


C'est un système intéressant et qui a laissé une trace profonde dans l'histoire ; cependant, la profusion de "chiffres" (69 !!), la difficulté de lecture et l'**absence de zéro opératoire** limite les capacités opératoires de ce système.
En outre, il n'y a pas de moyen de différencier les nombres décimaux des entiers. 

---

Le système maya du ["compte long"](#section_1_2), est également un système positionnel. 

## <span style="color: blue">IV.2 Le système positionnel décimal indo-arabe</span> <a class="anchor" id="section_4_2"></a>

!!! danger "Système décimal indo-arabe"

    "Notre" système de numération écrite, le [système décimal indo-arabe](https://fr.wikipedia.org/wiki/Système_de_numération_indo-arabe) hérite de plusieurs des propriétés vus précédemment : 

    * sa base décimale régulière et les dix chiffres utilisés sont des symboles de dimensions équivalentes, **ce qui facilite l'écriture et les opérations**.

    ![image.png](attachment:image.png)

    FIG.13 - Généalogie des numérations brahmi, gwalior, sanskrit-dévanagari et arabes, [Wikipedia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_num%C3%A9ration_indo-arabe#)

    * la notation positionnelle est sans ambiguïté grâce à l'invention du zéro comme chiffre et comme nombre.


Des progrès réalisés par les mathématiciens indiens retenons: 

* l'**invention du zéro** entre 300 et 600 après J-C, probablement sous influence babylonienne (apparition du zéro positionnel de manière ponctuelle dans certains écrits babyloniens). Dans le nombre 103, le symbole 0 signifie l’absence de dizaine. S’il n’y avait pas de symbole pour indiquer cette absence, on risquerait de confondre 1 3 et 13 !


![](https://upload.wikimedia.org/wikipedia/commons/8/83/Bakhshali_manuscript_zero_detail.jpg)
FIG.14 - Manuscrit de Bakhsali, peut-être le plus ancien écrit montrant l'utilisation du zéro, [Wikipedia](https://fr.wikipedia.org/wiki/Manuscrit_de_Bakhshali)
      
* Les **règles de manipulation des nombres avec signe** : Brahmagupta (VIIème siècle), 
 
* Les **règles d’utilisation du zéro « sunya »** : Sridhara (850-950) , symbole 0.

Le système indien est adopté par les arabes, qui le transmettront à l’Occident avec leurs propres apports.

Sous l'influence de l'Europe, de nombreux pays adoptent ce système, dont vous avez appris depuis votre plus tendre enfance les bénéfices pour les opérations élémentaires !

# <span style="color: red"><u>Conclusion</u></span> <a class="anchor" id="conclusion"></a>

Dans la conception moderne, un bon système de numération doit permettre de transmettre des informations numériques et d’effectuer des opérations le plus facilement et le plus sûrement possible.

[Carte mentale ](https://capytale2.ac-paris.fr/web/sites/default/files/2023/07-01/22-46-37/markmap%285%29.html)

## <span style="color: blue">Ressources supplémentaires</span> <a class="anchor" id="ressources"></a>

[Depuis quand savons nous compter ?, Pour la science](https://www.pourlascience.fr/sd/prehistoire/depuis-quand-savons-nous-compter-24056.php)

[Numération avec l'aide du corps humain](http://villemin.gerard.free.fr/Wwwgvmm/Numerati/CorpsHum.htm)


« Les nombres », Ebbinghaus et alii, Vuibert

« Histoire Universelle de la mesure », F. Jedrzejewski, Ellipses.

https://www.math93.com/histoire-des-maths/histoire-des-nombres/154-histoire-des-nombres.html

https://fr.wikipedia.org/wiki/Système_de_numération_indo-arabe

https://fr.wikipedia.org/wiki/Chiffres_arabes

https://fr.wikipedia.org/wiki/Notation_positionnelle


Patrice MARIE-JEANNE

Dernière modification de l'activité : 12/09/2023

Les activités partagées sur <a href="https://capytale2.ac-paris.fr/web/accueil"><strong>Capytale</strong></a> sont sous licence [Creative Commons](https://creativecommons.org/licenses/by-sa/3.0/fr/)

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

