# Représentation des données { "text-align:center; color: #32cd32; font-size:250% }

!!! abstract "Notions abordées"
    * Types et valeurs de base :
        * [Écriture d’un entier positif dans une base b ⩾ 2](numeration/index_num.md)
        * [Représentation binaire d’un entier relatif]()
        * [Représentation approximative des nombres réels : notion de nombre flottant]()
        * [Logique booléenne : valeurs, opérateurs, expressions]() 
        * [Représentation d’un texte en machine. Exemples des encodages ASCII, ISO-8859-1, Unicode]()
    * Types construits : 
        * [p-uplets. p-uplets nommés]()  
        * [Tableau indexé, tableau donné en compréhension]()
        * [Dictionnaires par clés et valeurs]() 

## Carte mentale { "text-align:center; color: #32cd32; font-size:250% }

![Représentation des données](mermaid-diagram-2023-07-03-115602.svg)

## Évaluation { "text-align:center; color: #32cd32; font-size:250% }

critères d'évaluations etc
