# <span style="color: #E49B0F">Algorithmique</span>

!!! abstract "Notions abordées"
    * [Parcours séquentiel d’un tableau]()
    * [Tris par insertion, par sélection]()
    * [Algorithme des k plus proches voisins]()
    * [Recherche dichotomique dans un tableau trié]()
    * [Algorithmes gloutons]()  

## Carte mentale 

## Évaluation 

critères d'évaluations etc
