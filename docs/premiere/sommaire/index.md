# <span style="color: #E49B0F">Thèmes du programme officiel</span>

Le [programme officiel de première NSI](https://eduscol.education.fr/document/30007/download)

<div class="image-map-container">
    <img src="carte_mentale_1ereNSI.svg" width="646" height="257" usemap="#map" />
 <div class="map-selector"></div>
 
<map name="map" id="map">
 <area id="algo" shape="rect" coords="298,11,414,48" href="../algo"/>
 <area id="archi_os" shape="rect" coords="52,34,260,89" href="../archi_os"/>
 <area id="lang_prog" shape="rect" coords="10,138,227,172" href="../langages_programmation"/>   
 <area id="rep_données" shape="rect" coords="190,190,343,247" href="../representation_donnees"/>
 <area id="ihm_web" shape="rect" coords="381,164,597,219" href="../ihm_web"/>
 <area id="donnees_tables" shape="rect" coords="430,63,634,116" href="../donnees-en-tables"/>  
</map>
</div>

# <span style="color: lime green"> 🗓️ Progression</span>

À venir




