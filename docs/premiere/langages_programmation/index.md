# <span style="color: #E49B0F">Langages et programmation</span>

!!! abstract "Notions abordées"
    * [Constructions élémentaires]()
    * [Diversité et unité des langages de programmation]()
    * [Fonctions : spécification](https://capytale2.ac-paris.fr/web/c/f602-644775)
    * [Mise au point des programmes](https://capytale2.ac-paris.fr/web/c/b148-373438)  
    * [Utilisation de bibliothèques](https://capytale2.ac-paris.fr/web/c/37f8-1233193)

## Carte mentale 

## Évaluation 

critères d'évaluations etc
