# Essais annotations

Lorem ipsum dolor sit amet, (1) consectetur adipiscing elit.
{ .annotate }

1. :man_raising_hand: I'm an annotation! I can contain `code`, __formatted
    text__, images, ... basically anything that can be expressed in Markdown.

!!! note annotate "Phasellus posuere in sem ut cursus (1)"

    Lorem ipsum dolor sit amet, (2) consectetur adipiscing elit. Nulla et
    euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo
    purus auctor massa, nec semper lorem quam in massa.

1. :man_raising_hand: I'm an annotation!
2. :woman_raising_hand: I'm an annotation as well!

=== "Tab 1"

    Lorem ipsum dolor sit amet, (1) consectetur adipiscing elit.
    { .annotate }

    1.  :man_raising_hand: I'm an annotation!

=== "Tab 2"

    Phasellus posuere in sem ut cursus (1)
    { .annotate }

    1.  :woman_raising_hand: I'm an annotation as well!

Text can be {--deleted--} and replacement text {++added++}. This can also be
combined into {~~one~>a single~~} operation. {==Highlighting==} is also
possible {>>and comments can be added inline<<}.

{==

Formatting can also be applied to blocks by putting the opening and closing
tags on separate lines and adding new lines between the tags and the content.

==}

- ==This was marked==
- ^^This was inserted^^
- ~~This was deleted~~
- H~2~O
- A^T^A
++ctrl+alt+del++

:smile:

$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$

[Hover me](https://example.com "I'm a tooltip!")

:material-information-outline:{ title="Important information" }

The HTML specification is maintained by the W3C.

*[HTML]: Hyper Text Markup Language
*[W3C]: World Wide Web Consortium

`Lorem ipsum dolor sit amet`

:   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
    tellus non sem sollicitudin, quis rutrum leo facilisis.

`Cras arcu libero`

:   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
    ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

    Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
    Nam vulputate tincidunt fringilla.
    Nullam dignissim ultrices urna non auctor.

- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
    * [x] In hac habitasse platea dictumst
    * [x] In scelerisque nibh non dolor mollis congue sed et metus
    * [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque

``` py
import tensorflow as tf
```

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```

``` mermaid
sequenceDiagram
  autonumber
  Alice->>John: Hello John, how are you?
  loop Healthcheck
      John->>John: Fight against hypochondria
  end
  Note right of John: Rational thoughts!
  John-->>Alice: Great!
  John->>Bob: How about you?
  Bob-->>John: Jolly good!
```

??? note "se déroule en cliquant dessus"

    Ma note indentée

!!! note inline "Note à gauche"

    Texte de la note indenté

Le texte non indenté se place à droite de la note.  
Il est en dehors de l'admonition.  
Il doit être assez long  
...

[Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/){ .md-button target="_blank" rel="noopener" }

<div class="centre">
<iframe 
src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" 
width="640" height="360" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

???+ question

    Voici diverses propositions

    === "Cocher la ou les affirmations correctes"

        - [ ] Proposition 1
        - [ ] Proposition 2
        - [ ] Proposition 3
        - [ ] Proposition 4

    === "Solution"

        - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
        - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
        - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
        - :x: ~~Proposition 4~~ Optionnel : Faux car ... 

```python 
note = float(input("Saisir votre note : "))
if note >= 16:  # (1)
    print("TB")
elif note >= 14:  # (2)
    print("B")
elif note >= 12:  # (3)
    print("AB")
elif note >= 10:
    print("reçu")
else:
    print("refusé")
```

1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

2. :warning: `elif` signifie **sinon si**.

3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"


```mermaid
    %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
    flowchart TB
        n0(15) --> n1(6)
        n1 --> n3(1)
        n1 --> n4(10)
        n0 --> n2(30)
        n2 --> n8[Null]
        n2 --> n5(18)
        n5 --> n6(16)
        n5 --> n7(25)
```

[Subscribe to our newsletter](#){ .md-button }

[Subscribe to our newsletter](#){ .md-button .md-button--primary }

[Send :fontawesome-solid-paper-plane:](#){ .md-button }


---
tags: template
---

# Flashcards

1.
    - Question
    - Réponse
2.
    - Lorem ipsum ?
    - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non diam ut quam vehicula efficitur sit amet vitae nisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed at rutrum dui.
3.
    - Question
    - Réponse


<style>ol,ul{list-style-type:none}#doc ul li{left:0;background-color:#fff;}ol li{background-color:transparent;border-radius:20px;width:500px;height:200px;border:none;margin-bottom:3em;perspective:1000px;display:flex;align-items:center;justify-content:center}#doc ul{position:relative;width:100%;height:100%;transition:transform .8s;transform-style:preserve-3d}ol li:hover>ul{transform:rotateY(180deg)}ol li>ul li:first-of-type,ol li>ul li:nth-of-type(2){position:absolute;width:100%;height:100%;-webkit-backface-visibility:hidden;backface-visibility:hidden}ol li>ul li:first-of-type{color:#000;font-weight:600;font-size:24px}ol li>ul li:nth-of-type(2){color:#000;transform:rotateY(180deg)}#doc ul li{padding:3em;overflow:hidden;border:5px solid #005596}</style>