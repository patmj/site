#TP

#####   tp diviser pour régner images :http://www.monlyceenumerique.fr/nsi_terminale/a/a3_div_regner.php#####
import os
import time
from PIL import Image

##EX. 5.1
im = Image.open("image.jpeg")
largeur, hauteur = im.size

im.show()

##EX. 5.2 : largeur = 1080 , hauteur = 720

##EX. 5.3 : (228, 234, 250)
im.getpixel((100,100))

#EX. 5.4 on crée un carré central avec la RGB (25,153,89). 
for x in range(490, 590):
    for y in range(310, 410):
        im.putpixel((x,y),(25, 153 ,89))

im.save("image2.jpeg", "jpeg")#sauvegarde de l'image modifiée

#EX. 5.5
with Image.open("image.jpeg") as im:

    # Provide the target width and height of the image
    (width, height) = (im.width // 2, im.height // 2)
    im_resized = im.resize((width, height))

print(im_resized.size)

#Ex.6

def rotation_naive (image, n):
    """affiche l'image retournée de 90° dans 
    le sens des aiguilles d'une montre.
    image est une chaîne de caractères = le nom de l'image; n sa taille (image carrée)
    """
    im1 = Image.open(image)
    im2 = Image.open(image)
    for x in range(n):
        for y in range(n):
            im2.putpixel((x, y), im1.getpixel((y, n -1 - x)))
    im2.show()

#test avec l'image recalibré
with Image.open("image.jpeg") as im:
    im_carre = im.resize((900, 900))

#im_carre.show()
im_carre.save("image_carre.jpeg", "jpeg")
rotation_naive("image_carre.jpeg", 900)

#Ex.7
#complexité: quadratique selon n = O(n^2)

#Ex.8
#diviser pour régner

def echangePixel(im, x1, y1, x2, y2):
    """échange la valeur des pixels de coordonnées (x1,y1) et (x2,y2) 
    dans l'image au format PIL.
    """
    (r, g, b) = im.getpixel((x1, y1))
    im.putpixel((x1, y1), im.getpixel((x2, y2)))
    im.putpixel((x2, y2), (r, g, b))

#Ex.9

def echangeCarre(im, x1, y1, x2, y2, n):
    """échange des parties de l'image carrée de taille n dont les coordonnées 
    de leur premier pixel est (x1,y1) et (x2,y2)
    """
    for i in range(n):
        for j in range(n):
            echangePixel(im, x1 + i, y1 + j, x2 + i, y2 + j)


#test: on crée une image3 qu'on va modifier 
with Image.open("image.jpeg") as im3 :
    echangeCarre(im3, 300, 400, 600, 500, 100)
    im3.show()
    im3.save("image3.jpeg", "jpeg")


#Ex.10
#il faut faire trois échanges : A et B, B et C, B et D par exemple

#Ex.11
# coordonnées du premier pixel de 4 carrés de taille m
# carré 1 : (x, y)
# carré 2 : (x + m, y)
# carré 3 : (x, y + m)
# carré 4 : (x + m, y + m)

#Ex.12
def tourneCarre(im, x, y, n) :
    """récursive, tourne d'un quart en sens horaire le carré de 
    l'image de taille n (avec n puissance de 2) dont le premier pixel 
    a pour coordonnées (x,y)
    """ 
    #cas de base: région réduite à un pixel, rien à faire
    if n == 1 :
        return
    #sinon on peut diviser le carré en 4 régions 
    else:
        n = n // 2 
        tourneCarre(im, x, y, n)
        tourneCarre(im, x + n, y, n)
        tourneCarre(im, x, y + n, n)
        tourneCarre(im, x + n, y + n, n)
    #il faut ensuite échanger les 4 régions comme vu plus haut
    echangeCarre(im, x, y, x + n, y , n)
    echangeCarre(im, x, y, x, y + n, n)
    echangeCarre(im, x, y + n, x + n, y + n, n)

#test
with Image.open("image.jpeg") as im4 :
    im4_carre = im4.resize((1024, 1024))
    tourneCarre(im4_carre, 0, 0, 1024)
    im4_carre.show()

#Ex.13
def rotation(image, n):
    """retourne la rotation de l'image enregistrée sous le nom image
    n est la taille du carre et doit être une puissance de deux pour
    obtenir le résultat souhaité.
    La sortie est une nouvelle image PIL
    """
    with Image.open(image) as im :
        #au cas où
        im_carre = im.resize((n, n))
        tourneCarre(im_carre, 0, 0, n)
    return im_carre
    
#test
with Image.open("image.jpeg") as im4 :
    im4_carre = im4.resize((1024, 1024))
    im4_carre.save("image4.jpeg", "jpeg")

im5 = rotation("image4.jpeg", 1024)
im5.show()

#################   Tests de performance   ##################


t1 = time.time()
# exécuter fonction
t2 = time.time()
tps_ex = t2 - t1


