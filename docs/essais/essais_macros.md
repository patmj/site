Question 1 : $2^3=$

{{ multi_qcm(
    ["Compléter : $2^3=$", ["9", "8", "Je ne sais pas", "6"],[2],],
    multi = False,
    qcm_title = "Puissances",
    shuffle = True,
) }}

???+ question

    Compléter le script ci-dessous :
    Mettre le fichier python dans un dossier scripts


    {{ IDE('../scripts/nbrepremiers.py') }}

??? success "Solution"
    On peut donner la solution sous forme de code à copier :
    ```python
    # Code de la solution
    ```       