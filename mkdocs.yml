# À modifier avec le nom et la description du site
site_name: "Site des ressources NSI et SNT du LGT Clemenceau"
site_description: Un modèle avec mkdocs

# À modifier, avec votre nom et la licence choisie
copyright: 
  <a href="https://forge.apps.education.fr/patmj/" target="_blank">Patrice MARIE-JEANNE</a>
  &copy; 2023 sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank">CC BY-SA 4.0</a> 
  <img src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" alt="logo CC BY-SA 4.0" align="right">
  <br> Illustrations par <a href="https://undraw.co/" target="_blank">UnDraw</a>

# À modifier, avec votre mail
extra:
    social:
        - icon: fontawesome/regular/envelope
          link: mailto:patrice-florent.marie-jeanne@ac-montpellier.fr
          name: Écrire à l'auteur
        - icon: fontawesome/brands/gitlab
          link: !ENV [CI_PROJECT_ROOT_NAMESPACE]
          name: forge.apps.education

# Éventuellement à modifier avec votre thème de couleurs
theme:
    favicon: assets/favicon.ico
    icon:
      logo: material/ghost
    #custom_dir: my_theme_customizations/
    name: pyodide-mkdocs-theme
    font: false                     # RGPD ; pas de fonte Google
    #language: fr                    # français
    palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: black
        accent: deep orange
        toggle:
          icon: material/weather-sunny
          name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: black
        accent: green
        toggle:
          icon: material/weather-night
          name: Passer au mode jour
    features:
        #- navigation.instant
        #- navigation.tabs   Pour avoir le menu vertical il fallait supprimer ça
        - navigation.top
        - toc.integrate
        - header.autohide
        - content.code.annotate   # Pour les annotations de code deroulantes avec +
    features:
        #- navigation.instant
        - navigation.tabs   #Pour avoir le menu vertical il faut supprimer ça
        - navigation.tabs.sticky
        - navigation.top
        - navigation.tracking
        - navigation.sections
        #- navigation.expand
        - navigation.indexes
        - navigation.footer
        - toc.integrate
        - header.autohide
        - content.code.copy
        - content.code.annotate   # Pour les annotations de code deroulantes avec +
    highlightjs: true
    hljs_languages:
      - yaml
      - python

# Fin de modifications à effectuer, ne pas toucher les lignes suivantes sauf si vous comprenez ce que vous faites.

site_url: !ENV [CI_PAGES_URL, "http://127.0.0.1:8000/"]
site_author: !ENV [CI_COMMIT_AUTHOR, "Nom d'auteur"]
repo_url: !ENV [CI_PROJECT_URL]
edit_uri: !ENV [EDIT_VARIABLE]

docs_dir: docs

markdown_extensions:
    - md_in_html
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
        anchor_linenums: true
        line_spans: __span
        pygments_lang_class: true
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji: # Émojis  :boom:
        emoji_index: !!python/name:material.extensions.emoji.twemoji
        emoji_generator: !!python/name:material.extensions.emoji.to_svg
    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format
    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 2

plugins:
  - awesome-pages:
      collapse_single_pages: true
  - search
  # - mkdocs-jupyter:
  #     ignore_h1_titles: True
  
  - tags:
      tags_file: tags.md
  - pyodide_macros:
      # Vous pouvez ajouter ici tout réglage que vous auriez ajouté concernant les macros:
      on_error_fail: true     # Il est conseillé d'ajouter celui-ci si vous ne l'utilisez pas.
      build:
        # python_libs:  # Pour avoir la tortue Python
        #   - turtle
        tab_to_spaces: 4  # Pour convertir les tabulations en 4 espaces
  
# En remplacement de mkdocs-exclude. Tous les fichiers correspondant aux patterns indiqués seront
# exclu du site final et donc également de l'indexation de la recherche.
# Nota: ne pas mettre de commentaires dans ces lignes !
exclude_docs: |
    **/*_REM.md
    **/*.py
    
#extra_javascript:
#- xtra/javascripts/mathjax-config.js       Supprimé pour MAJ pyodide             
#- https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  #- xtra/stylesheets/qcm.css ##       Supprimé pour MAJ pyodide  
  - xtra/stylesheets/ajustements.css  # ajustements

nav:
  - "🏡 Accueil": 
    - index.md
    - Prise en main : prise_en_main.md
  
  - Classes:
    - Terminale NSI:

      - Plan: terminale/sommaire/index.md

      - Algorithmique:
        - terminale/algo/index.md 

      - Architectures matérielles, systèmes d’exploitation et réseaux:
        - terminale/archi/index.md
      
      - Base de données :
        - terminale/BDD/index.md
       # Attention, il ne faut pas d'erreurs SQL dans le notebook -> solution:  notebook non exécuté (06/07/23)
      #   - BDD 1 Le modèle relationnel: terminale/BDD/nsi_tale_bdd1_modele_relationnel.md
      #   - BDD 2 Le langage SQL: terminale/BDD/nsi_tale_bdd2_bdd_sql.ipynb
      #   - BDD 3 Opérations avec SQL: terminale/BDD/nsi_tale_bdd3_sql_premier-pas_projections_restrictions_jointures.ipynb 
      #   - BDD 4 Les contraintes ACID pour les SGBD: terminale/BDD/nsi_tale_bdd4_sgbd-acid.ipynb
      #   - BDD 5 SGBD et Python: terminale/BDD/nsi_tale_bdd5_python_sqlite3eleve.ipynb 
      
      - Langages et programmation:
        - terminale/langages_programmation/index.md
      
      - Structures de données:
        - terminale/structure_donnees/index.md
      
      - Progression: terminale/Progression NSI_Tale.pdf

    - Première NSI:
    
      - Plan : premiere/sommaire/index.md
      
      - Algorithmique: 
        - premiere/algo/index.md
      
      - Architectures matérielles et systèmes d'exploitation:
        - premiere/archi_os/index.md
      
      - Interactions entre l'homme et la machine sur le web:
        - premiere/ihm_web/index.md

      - Langages et programmation:
        - premiere/langages_programmation/index.md
      
      - Représentation des données:
        - premiere/representation_donnees/index.md
      
      - Traitement de données en tables:
        - premiere/donnees-en-tables/index.md

    #- SNT:
      #- Internet:
      #- Le Web:
      #- Les réseaux sociaux:
      #- Les données structurées et leur traitement:
      #- Localisation, cartographie et mobilité:
      #- Informatique embarquée et objets connectés:
      #- La photographie numérique:

  #- Projets:
    #- Mini-projets:
    #- Les trophées NSI:
    #- Concours C'Génial:

  - Ressources:
    - Capytale: ressources/capytale.md
    - Concours et stages: ressources/concours.md 
    - Gestion de projets: ressources/gestion_projets.md
    - Orientation: ressources/orientation.md
    - Plateformes et sites d'apprentissage: ressources/plateformes.md
    - Programmes officiels : https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g   


  - Tutoriels: 
    - Créer un tutoriel: tutoriels/presentation_tutoriel.md
    - Capytale:
      - Admonitions: tutoriels/admonitions.md
    - Programmation:
      - Création de jeu avec pyxel : tutoriels/Creation_de_jeu_avec_pyxel_Arthur_Hiely.md
    - Outils collaboratifs:
      - Utilisation de Github Desktop: tutoriels/tuto_github_desktop.md
    - Jeux:
      - Minecraft, utilisation de la Redstone: tutoriels/redstone_minecraft.md
    #- IA:
    #- Réseaux, sécurisation des communications:
    #- Images, 3D:
    #- Sons:
    - Administration système:
      - Le gestionnaire de macros Touch Portal : tutoriels/tuto_portal_corentin_finou.md
    #- Robotique, IOT:

    - essais:
      # - Notebook page: essais/essai2.ipynb
      # - Python file: essais/essai.py
      - Modèle de frise chronologique: essais/Timeline.html
      - Annotations: essais/annotations_essais.md
      - Essais macros: essais/essais_macros.md

  
